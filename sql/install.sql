/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.6.23 : Database - icms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`icms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `icms`;

/*Table structure for table `b_role` */

DROP TABLE IF EXISTS `b_role`;

CREATE TABLE `b_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '角色名称',
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `super_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否拥有所有权限 0否 1是 默认0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `b_role` */

/*Table structure for table `b_settings_attr` */

DROP TABLE IF EXISTS `b_settings_attr`;

CREATE TABLE `b_settings_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `attr_key` varchar(50) NOT NULL DEFAULT '' COMMENT '键名',
  `attr_value` varchar(50) NOT NULL DEFAULT '' COMMENT '键值',
  `operator_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人id',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `b_settings_attr` */

insert  into `b_settings_attr`(`id`,`attr_key`,`attr_value`,`operator_id`,`create_time`,`update_time`,`del_flag`) values (1,'templatePath','default',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*Table structure for table `f_channel` */

DROP TABLE IF EXISTS `f_channel`;

CREATE TABLE `f_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父栏目id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `path` varchar(50) NOT NULL DEFAULT '' COMMENT '访问路径',
  `template_name` varchar(20) NOT NULL DEFAULT '' COMMENT '模板名称',
  `template_file` varchar(100) NOT NULL DEFAULT '' COMMENT '模板文件',
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT '优先级（排序）',
  `single_page_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '单页标识 0否 1是 默认0',
  `display_flag` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示 0否 1是 默认0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `f_channel` */

insert  into `f_channel`(`id`,`parent_id`,`name`,`path`,`template_name`,`template_file`,`priority`,`single_page_flag`,`display_flag`,`create_time`,`update_time`,`del_flag`) values (1,3,'第一级别_999','news','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(2,0,'第一级别_0333','enter','default','/channel/channel',5,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(3,15,'第二级别_11','even','default','/channel/channel',1,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(4,0,'第二级别_02','','default','/channel/channel',33,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(5,0,'第二级别_10','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(6,0,'第二级别_00','','default','/channel/channel',1,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(7,0,'第二级别_01','','default','/channel/channel',1,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(8,0,'第二级别_12','','default','/channel/channel',2,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(9,0,'第三级别_120','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(10,0,'第四级别_1200','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(11,1,'第五级别_12000','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(12,6,'第三级别_101','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(13,0,'唐唐唐唐唐唐','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(14,0,'5555555','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(15,12,'第三级别_020','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(16,0,'第六级别_120000','','default','/channel/channel',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(17,10,'的萨菲费十大发生的发生的发烧豆腐阿斯顿飞洒分手的','','','',0,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*Table structure for table `f_content` */

DROP TABLE IF EXISTS `f_content`;

CREATE TABLE `f_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_channel.id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_user.id',
  `type_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '内容类型 0普通 1图文 2焦点 3头条',
  `model_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `recommend_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '推荐标识 0否 1是 默认0',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT '标题',
  `title_color` varchar(10) NOT NULL DEFAULT '' COMMENT '标题颜色',
  `title_bold_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '标题加粗 0否 1是 默认0',
  `title_img` varchar(128) NOT NULL DEFAULT '' COMMENT '标题图片 用于列表展示时',
  `author` varchar(40) NOT NULL DEFAULT '' COMMENT '作者',
  `origin` varchar(40) NOT NULL DEFAULT '' COMMENT '来源',
  `origin_url` varchar(255) NOT NULL DEFAULT '' COMMENT '来源url',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '摘要',
  `issue_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '发布时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0审核中 1审核通过 2草稿 3回收站',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20000 DEFAULT CHARSET=utf8;

/*Data for the table `f_content` */

insert  into `f_content`(`id`,`channel_id`,`user_id`,`type_id`,`model_id`,`recommend_flag`,`title`,`title_color`,`title_bold_flag`,`title_img`,`author`,`origin`,`origin_url`,`summary`,`issue_time`,`status`,`create_time`,`update_time`,`del_flag`) values (1,4,78,0,0,0,'啊啊啊','',0,'','啊啊','','','啊','2015-01-20 18:34:17',1,'2015-01-20 18:34:17','0000-00-00 00:00:00',0),(2,2,78,0,0,0,'啊啊','',0,'','阿斯顿','','','盛大','2015-01-20 18:34:27',1,'2015-01-20 18:34:27','0000-00-00 00:00:00',0),(3,3,78,0,0,0,'asfsdsd','',0,'','sdfaasdf','','','sdf','2015-01-20 20:28:58',1,'2015-01-20 20:28:58','0000-00-00 00:00:00',0),(4,4,78,0,0,0,'阿斯蒂芬盛大','',0,'','的萨菲撒大幅','','','撒旦法盛大撒的','2015-01-20 21:04:40',1,'2015-01-20 21:04:40','0000-00-00 00:00:00',0),(5,4,78,0,0,0,'Java 高亮测试','',0,'','java','','','java','2015-01-20 21:05:58',1,'2015-01-20 21:05:58','0000-00-00 00:00:00',0),(6,4,78,0,0,0,'撒旦法萨芬','',0,'','撒的发生','','','撒旦法撒的','2015-01-21 22:36:43',1,'2015-01-21 22:36:43','0000-00-00 00:00:00',0),(7,0,0,0,0,0,'adfadfs','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(8,0,0,0,0,0,'sadf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(9,0,0,0,0,0,'f','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(10,0,0,0,0,0,'sdaf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(11,0,0,0,0,0,'sadf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(12,0,0,0,0,0,'sadf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(13,0,0,0,0,0,'sadf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(14,0,0,0,0,0,'asf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(15,0,0,0,0,0,'asdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(16,0,0,0,0,0,'asdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(17,0,0,0,0,0,'sdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(18,0,0,0,0,0,'asdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(19,0,0,0,0,0,'asf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(20,0,0,0,0,0,'asdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(21,0,0,0,0,0,'sadf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(22,0,0,0,0,0,'asfd','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(23,0,0,0,0,0,'asdf','',0,'','','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(24,5,78,0,0,0,'11111111111111111111111','',0,'','sdafsad','','','fsdfsadf','2015-03-07 14:21:48',1,'2015-03-07 14:21:48','0000-00-00 00:00:00',0),(19999,0,0,0,0,0,'','',0,'','tttt','','','','0000-00-00 00:00:00',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*Table structure for table `f_content_settings_can_del` */

DROP TABLE IF EXISTS `f_content_settings_can_del`;

CREATE TABLE `f_content_settings_can_del` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `type_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '内容类型 0普通 1图文 2焦点 3头条',
  `model_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'f_model.id',
  `recommend_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '推荐标识 0否 1是 默认0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_content_settings_can_del` */

/*Table structure for table `f_content_txt` */

DROP TABLE IF EXISTS `f_content_txt`;

CREATE TABLE `f_content_txt` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `content_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_content.id',
  `txt` longtext NOT NULL COMMENT '文章内容',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_id_uni` (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `f_content_txt` */

insert  into `f_content_txt`(`id`,`content_id`,`txt`,`create_time`,`update_time`,`del_flag`) values (1,1,'<p>&nbsp;啊盛大阿斯顿暗示盛大</p>','2015-01-20 18:34:17','0000-00-00 00:00:00',0),(2,2,'<p>撒的</p>','2015-01-20 18:34:27','0000-00-00 00:00:00',0),(3,3,'<p>asdf</p>','2015-01-20 20:28:58','0000-00-00 00:00:00',0),(4,4,'<p>士大夫撒大</p>','2015-01-20 21:04:41','0000-00-00 00:00:00',0),(5,5,'<pre class=\"brush:java;toolbar:false\">package&nbsp;com.itmuch.icms.controller.front.user;\r\n\r\nimport&nbsp;javax.annotation.Resource;\r\nimport&nbsp;javax.servlet.http.HttpServletRequest;\r\n\r\nimport&nbsp;org.apache.shiro.SecurityUtils;\r\nimport&nbsp;org.apache.shiro.authc.AuthenticationException;\r\nimport&nbsp;org.apache.shiro.authc.UsernamePasswordToken;\r\nimport&nbsp;org.apache.shiro.subject.Subject;\r\nimport&nbsp;org.slf4j.Logger;\r\nimport&nbsp;org.slf4j.LoggerFactory;\r\nimport&nbsp;org.springframework.stereotype.Controller;\r\nimport&nbsp;org.springframework.web.bind.annotation.RequestMapping;\r\nimport&nbsp;org.springframework.web.bind.annotation.RequestMethod;\r\n\r\nimport&nbsp;com.itmuch.icms.front.user.domain.User;\r\nimport&nbsp;com.itmuch.icms.front.user.service.IUserService;\r\n\r\n/**\r\n&nbsp;*&nbsp;会员注册\r\n&nbsp;*&nbsp;@author&nbsp;ITMuch\r\n&nbsp;*/\r\n@Controller\r\npublic&nbsp;class&nbsp;LoginController&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;private&nbsp;static&nbsp;final&nbsp;Logger&nbsp;LOGGER&nbsp;=&nbsp;LoggerFactory.getLogger&#40;LoginController.class&#41;;\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;@Resource\r\n&nbsp;&nbsp;&nbsp;&nbsp;private&nbsp;IUserService&nbsp;userService;\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;@RequestMapping&#40;value&nbsp;=&nbsp;&quot;/login&quot;,&nbsp;method&nbsp;=&nbsp;RequestMethod.GET&#41;\r\n&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;String&nbsp;login&#40;User&nbsp;user&#41;&nbsp;{\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&quot;default/user/login&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;}\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;@RequestMapping&#40;value&nbsp;=&nbsp;&quot;/login&quot;,&nbsp;method&nbsp;=&nbsp;RequestMethod.POST&#41;\r\n&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;String&nbsp;loginDeal&#40;User&nbsp;user,&nbsp;HttpServletRequest&nbsp;request&#41;&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UsernamePasswordToken&nbsp;token&nbsp;=&nbsp;new&nbsp;UsernamePasswordToken&#40;user.getUsername&#40;&#41;,&nbsp;user.getPassword&#40;&#41;&#41;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SecurityUtils.getSubject&#40;&#41;.login&#40;token&#41;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;catch&nbsp;&#40;AuthenticationException&nbsp;e&#41;&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOGGER.debug&#40;&quot;认证失败，账号或密码有误&quot;&#41;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;request.setAttribute&#40;&quot;loginError&quot;,&nbsp;&quot;账号或密码有误.&quot;&#41;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&quot;redirect:/member&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;}\r\n\r\n&nbsp;&nbsp;&nbsp;&nbsp;@RequestMapping&#40;value&nbsp;=&nbsp;&quot;/logout&quot;,&nbsp;method&nbsp;=&nbsp;RequestMethod.POST&#41;\r\n&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;void&nbsp;logout&#40;&#41;&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subject&nbsp;subject&nbsp;=&nbsp;SecurityUtils.getSubject&#40;&#41;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;&#40;subject.isAuthenticated&#40;&#41;&#41;&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;subject.logout&#40;&#41;;&nbsp;//&nbsp;session&nbsp;会销毁，在SessionListener监听session销毁，清理权限缓存\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}\r\n&nbsp;&nbsp;&nbsp;&nbsp;}\r\n\r\n}</pre><p><br/></p>','2015-01-20 21:05:58','0000-00-00 00:00:00',0),(6,6,'<p>的撒的撒的的阿斯顿</p>','2015-01-21 22:36:44','0000-00-00 00:00:00',0),(7,0,'<p>sdafsdfasdfsadfsdaf</p>','2015-03-07 14:21:48','0000-00-00 00:00:00',0),(9,44444446,'<p>课后偶偶偶偶偶偶偶</p>','2015-03-12 21:33:33','0000-00-00 00:00:00',0);

/*Table structure for table `f_group` */

DROP TABLE IF EXISTS `f_group`;

CREATE TABLE `f_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '会员组名称',
  `need_captcha` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否需要验证码',
  `need_check` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否需要审核',
  `upload_per_day` int(11) NOT NULL DEFAULT '0' COMMENT '每日允许上传KB',
  `upload_max_file` int(11) NOT NULL DEFAULT '0' COMMENT '每个文件最大KB',
  `allow_upload_suffex` varchar(100) NOT NULL DEFAULT '' COMMENT '允许上传的后缀',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `f_group` */

insert  into `f_group`(`id`,`name`,`need_captcha`,`need_check`,`upload_per_day`,`upload_max_file`,`allow_upload_suffex`,`create_time`,`update_time`,`del_flag`) values (1,'1级会员',0,0,0,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*Table structure for table `f_group_channel` */

DROP TABLE IF EXISTS `f_group_channel`;

CREATE TABLE `f_group_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_id` int(11) DEFAULT '0' COMMENT 'f_group.id',
  `channel_id` int(11) DEFAULT '0' COMMENT 'f_channel.id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `f_group_channel` */

insert  into `f_group_channel`(`id`,`group_id`,`channel_id`) values (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5);

/*Table structure for table `f_user` */

DROP TABLE IF EXISTS `f_user`;

CREATE TABLE `f_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户组id',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) NOT NULL DEFAULT '' COMMENT '盐',
  `email` varchar(500) NOT NULL DEFAULT '' COMMENT '邮箱',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态 0未激活 1已激活 2查封 默认0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

/*Data for the table `f_user` */

insert  into `f_user`(`id`,`group_id`,`username`,`password`,`salt`,`email`,`status`,`create_time`,`update_time`,`del_flag`) values (78,1,'eacdy','0e76ced18c011b3bc2bdc6ee02d4e737','88e753816905808b','e@12.com',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(79,0,'ttttt','297d1d00fdc65b12440ff05e168b5f2c','e9ef0e2e7bbcdc2f','ttt@12.com',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(80,0,'cccsadfsadf','4e78ee7480bb4fc7a4ba74b774d97d8d','4dcd6830b57aa4bd','ccc@c.com',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),(81,0,'ccxx','b7830747877f8994154f9c5017e4dfad','545500dc6623a596','xx@1.com',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0);

/*Table structure for table `f_user_expand` */

DROP TABLE IF EXISTS `f_user_expand`;

CREATE TABLE `f_user_expand` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_user.id',
  `reg_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '注册时间',
  `reg_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '注册IP',
  `last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后登录时间',
  `last_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `upload_total_size` int(11) NOT NULL DEFAULT '0' COMMENT '上传文件总大小KB',
  `upload_size` int(11) NOT NULL DEFAULT '0' COMMENT '上传文件大小',
  `upload_date` date NOT NULL DEFAULT '0000-00-00' COMMENT '文件上传日期',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_user_expand` */

/*Table structure for table `f_user_info` */

DROP TABLE IF EXISTS `f_user_info`;

CREATE TABLE `f_user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'f_user.id',
  `realname` varchar(50) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '性别 0保密 1男 2女 默认0',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `signature` varchar(256) NOT NULL DEFAULT '' COMMENT '个性签名',
  `qq` varchar(13) NOT NULL DEFAULT '' COMMENT 'QQ',
  `tel` varchar(18) NOT NULL DEFAULT '' COMMENT '电话',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `img_url` varchar(256) NOT NULL DEFAULT '' COMMENT '头像地址',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0否 1是 默认0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_user_info` */

/*Table structure for table `test_user` */

DROP TABLE IF EXISTS `test_user`;

CREATE TABLE `test_user` (
  `id` int(11) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `test_user` */

insert  into `test_user`(`id`,`city`,`age`,`gender`) values (1,'BeiJing',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
