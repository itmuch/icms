package com.itmuch.icms.freemarker.directive.obj;

public class Pager {
    /**
     * 数据
     */
    private Object data;

    /**
     * 总共多少条数据
     */
    private Integer totalCount;

    /**
     * 总共多少页
     */
    private Integer totalPageNo;

    /**
     * 上一页页码
     */
    private Integer prePageNo;

    /**
     * 下一页页码
     */
    private Integer nextPageNo;

    /**
     * 当前页码
     */
    private Integer pageNo;

    /**
     * 路径
     */
    private String path;

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPageNo() {
        return this.totalPageNo;
    }

    public void setTotalPageNo(Integer totalPageNo) {
        this.totalPageNo = totalPageNo;
    }

    public Integer getPrePageNo() {
        return this.prePageNo;
    }

    public void setPrePageNo(Integer prePageNo) {
        this.prePageNo = prePageNo;
    }

    public Integer getNextPageNo() {
        return this.nextPageNo;
    }

    public void setNextPageNo(Integer nextPageNo) {
        this.nextPageNo = nextPageNo;
    }

    public Integer getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
