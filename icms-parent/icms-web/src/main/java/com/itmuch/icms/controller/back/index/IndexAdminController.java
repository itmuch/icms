package com.itmuch.icms.controller.back.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class IndexAdminController {
    @RequestMapping("")
    public String index() {
        // 暂时拿这个文件顶一下
        return "admin/index";
    }
}
