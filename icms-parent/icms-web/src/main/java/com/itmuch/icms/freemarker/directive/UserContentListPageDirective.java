package com.itmuch.icms.freemarker.directive;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.itmuch.core.constants.SessionConstants;
import com.itmuch.core.util.DozerUtil;
import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.service.IChannelService;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.content.vo.ContentCommonVo;
import com.itmuch.icms.freemarker.directive.constants.DirectiveConstants;
import com.itmuch.icms.freemarker.directive.obj.Pager;
import com.itmuch.icms.freemarker.directive.util.DirectiveUtils;
import com.itmuch.icms.util.UrlUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 本类用于查询指定用户指定页的发布文章的信息（用于会员中心，展示投稿列表）
 * @author ITMuch
 */
public class UserContentListPageDirective implements TemplateDirectiveModel {
    @Resource
    private IContentService contentService;
    @Resource
    private IChannelService channelService;

    @SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        Integer userId = (Integer) attr.getRequest().getSession().getAttribute(SessionConstants.USER_ID);
        // 页码
        Integer pageNo = DirectiveUtils.getIntByEnv("pageNo", env);
        // 路径
        String path = DirectiveUtils.getStringByEnv("path", env);

        // 单页显示条数，默认10条
        Integer limit = DirectiveUtils.getInt("limit", params);
        limit = limit == null ? 10 : limit;

        PageList<Content> list = (PageList<Content>) this.contentService.selectByUserIdPaged(userId, new PageBounds(pageNo, limit));

        List<ContentCommonVo> voList = new ArrayList<ContentCommonVo>();
        // 取得栏目名称及其链接
        if ((list != null) && !list.isEmpty()) {
            for (Content content : list) {
                ContentCommonVo vo = DozerUtil.map(content, ContentCommonVo.class);
                String url = UrlUtil.getContentUrl(content.getId());
                String channelUrl = UrlUtil.getChannelUrl(content.getChannelId());
                vo.setUrl(url);
                vo.setChannelUrl(channelUrl);
                // 未来引入缓存后，可以优化。
                Channel channel = this.channelService.selectById(content.getChannelId());
                String channelName = channel != null ? channel.getName() : "";
                vo.setChannelName(channelName);
                voList.add(vo);
            }
        }

        Pager pager = new Pager();
        pager.setData(voList);
        pager.setPageNo(pageNo);
        pager.setPath(path);
        pager.setNextPageNo(list.getPaginator().getNextPage());
        pager.setPrePageNo(list.getPaginator().getPrePage());
        pager.setTotalCount(list.getPaginator().getTotalCount());
        pager.setTotalPageNo(list.getPaginator().getTotalPages());

        TemplateModel wrap = DEFAULT_WRAPPER.wrap(pager);
        env.setVariable(DirectiveConstants.CONTENT_LIST_PAGE, wrap);
        body.render(env.getOut());
    }
}
