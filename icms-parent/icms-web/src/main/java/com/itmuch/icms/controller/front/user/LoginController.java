package com.itmuch.icms.controller.front.user;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itmuch.icms.user.domain.User;
import com.itmuch.icms.user.service.IUserService;

/**
 * 会员注册
 * @author ITMuch
 */
@Controller
public class LoginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Resource
    private IUserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(User user) {

        return "default/user/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginDeal(User user, HttpServletRequest request) {
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
        try {
            SecurityUtils.getSubject().login(token);
        } catch (AuthenticationException e) {
            LOGGER.debug("认证失败，账号或密码有误");
            request.setAttribute("loginError", "账号或密码有误.");
        }

        return "redirect:/member";
    }

}
