package com.itmuch.icms.freemarker.directive;

import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.itmuch.core.util.DozerUtil;
import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.service.IChannelService;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.content.vo.ContentCommonVo;
import com.itmuch.icms.content.vo.ContentSearchVo;
import com.itmuch.icms.freemarker.directive.constants.DirectiveConstants;
import com.itmuch.icms.freemarker.directive.obj.Pager;
import com.itmuch.icms.freemarker.directive.util.DirectiveUtils;
import com.itmuch.icms.util.StringUtil;
import com.itmuch.icms.util.UrlUtil;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * iContentListPage 标签
 * @author ITMuch
 *
 */
public class ContentListPageDirective implements TemplateDirectiveModel {
    @Resource
    private IContentService contentService;
    @Resource
    private IChannelService channelService;

    @SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        // 页码
        Integer pageNo = DirectiveUtils.getIntByEnv("pageNo", env);
        // 路径
        String path = DirectiveUtils.getStringByEnv("path", env);
        // 排序方式
        Integer orderBy = DirectiveUtils.getInt("orderBy", params);
        PageList<Content> list = null;
        String channelIds = DirectiveUtils.getString("channelIds", params);
        Boolean recommend = DirectiveUtils.getBool("recommend", params);

        Integer typeId = DirectiveUtils.getInt("typeId", params);
        Integer img = DirectiveUtils.getInt("img", params);
        String excludeIds = DirectiveUtils.getString("excludeIds", params);

        // 单页显示条数，默认10条
        Integer limit = DirectiveUtils.getInt("limit", params);
        limit = limit == null ? 10 : limit;

        Integer containChild = DirectiveUtils.getInt("containChild", params);
        list = (PageList<Content>) this.contentService.selectByConditionPaged(new ContentSearchVo(StringUtil.parseStringToIntegerArray(channelIds),
                recommend, orderBy, typeId, img, StringUtil.parseStringToIntegerArray(excludeIds), containChild, 1), new PageBounds(pageNo, limit));

        // 取得路径栏目及路径名称
        List<ContentCommonVo> voList = new ArrayList<ContentCommonVo>();
        if ((list != null) && !list.isEmpty()) {
            for (Content content : list) {
                ContentCommonVo vo = DozerUtil.map(content, ContentCommonVo.class);
                String url = UrlUtil.getContentUrl(content.getId());
                String channelUrl = UrlUtil.getChannelUrl(content.getChannelId());
                vo.setUrl(url);
                vo.setChannelUrl(channelUrl);
                // 未来引入缓存后，可以优化。
                Channel channel = this.channelService.selectById(content.getChannelId());
                String channelName = channel != null ? channel.getName() : "";
                vo.setChannelName(channelName);
                voList.add(vo);
            }
        }

        Pager pager = new Pager();
        pager.setData(voList);
        pager.setPageNo(pageNo);
        pager.setPath(path);
        pager.setNextPageNo(list.getPaginator().getNextPage());
        pager.setPrePageNo(list.getPaginator().getPrePage());
        pager.setTotalCount(list.getPaginator().getTotalCount());
        pager.setTotalPageNo(list.getPaginator().getTotalPages());

        TemplateModel wrap = DEFAULT_WRAPPER.wrap(pager);
        env.setVariable(DirectiveConstants.CONTENT_LIST_PAGE, wrap);
        body.render(env.getOut());
    }
}
