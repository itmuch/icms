package com.itmuch.icms.freemarker.directive;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.itmuch.icms.settings.domain.SettingsAttr;
import com.itmuch.icms.settings.service.ISettingsAttrService;

/**
 * 根据后台配置，动态加载模板路径。
 * @author ITMuch
 *
 */
@Deprecated
// 待删除
public class CustomFreeMarkerConfigurer extends FreeMarkerConfigurer {

    private ISettingsAttrService settingsAttrService;

    @Override
    public void setTemplateLoaderPath(String templateLoaderPath) {
        SettingsAttr value = this.settingsAttrService.selectBykey("templatePath");
        StringBuffer buff = new StringBuffer().append(templateLoaderPath).append(value.getAttrValue()).append("/");
        super.setTemplateLoaderPaths(new String[] { buff.toString() });
    }

    public ISettingsAttrService getSettingsAttrService() {
        return this.settingsAttrService;
    }

    public void setSettingsAttrService(ISettingsAttrService settingsAttrService) {
        this.settingsAttrService = settingsAttrService;
    }

}
