package com.itmuch.icms.controller.back.channel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itmuch.core.util.DozerUtil;
import com.itmuch.core.web.converter.Result;
import com.itmuch.icms.channel.ChannelVo;
import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.service.IChannelService;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.util.TemplateUtil;
import com.itmuch.icms.util.UrlUtil;

@Controller
@RequestMapping("admin/channel")
public class ChannelAdminController {
    @Resource
    private IChannelService channelService;
    @Resource
    private IContentService contentService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(HttpServletRequest req) {
        List<Channel> list = this.channelService.selectChannelAll();

        List<ChannelVo> voList = null;
        if ((list != null) && !list.isEmpty()) {
            voList = new ArrayList<ChannelVo>();
            for (Channel channel : list) {
                ChannelVo vo = DozerUtil.map(channel, ChannelVo.class);
                vo.setUrl(UrlUtil.getChannelUrl(channel.getId()));
                voList.add(vo);
            }
        }
        req.setAttribute("channelList", voList);
        return "admin/channel/channel";
    }

    /**
     * 编辑栏目排序
     * @param ids 栏目id列表
     * @param priority 栏目排序列表
     * @return 栏目页
     */
    @RequestMapping(value = "/edit-priority", method = RequestMethod.POST)
    public String updatePriority(Integer[] ids, Integer[] priority) {
        this.channelService.updatePriority(ids, priority);
        return "redirect:/admin/channel";
    }

    /**
     * 通过id，删除栏目
     * @param id id
     * @param req 请求
     * @return 栏目页
     */
    @RequestMapping(value = "/del-by-id/{id}", method = RequestMethod.GET)
    public String delById(@PathVariable Integer id, HttpServletRequest req) {
        Integer childChannelCount = this.channelService.selectChildCountbyId(id);
        Integer contentCount = this.contentService.selectCountbyChannelId(id);
        if ((childChannelCount > 0) || (contentCount > 0)) {
            req.setAttribute("msg", "该栏目下有子栏目或有内容");
            return TemplateUtil.adminError();
        }
        this.channelService.deleteById(id);

        return "redirect:/admin/channel";
    }

    @RequestMapping(value = {"/add/{parentId}"}, method = RequestMethod.GET)
    public String addPage(@PathVariable Integer parentId, HttpServletRequest req) {
        List<Channel> list = this.channelService.selectChannelAll();
        req.setAttribute("channelList", list);
        return "admin/channel/channel_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String save(Channel channel) {
        this.channelService.insert(channel);
        return "redirect:/admin/channel";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editPage(@PathVariable Integer id, HttpServletRequest req) {
        Channel channel = this.channelService.selectById(id);
        List<Channel> list = this.channelService.selectChannelAll();
        req.setAttribute("channel", channel);
        req.setAttribute("channelList", list);
        return "admin/channel/channel_edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editPage(Channel channel, HttpServletRequest req) {
        // TODO 不允许将栏目的parentid 设成栏目的子栏目/孙子栏目..等的id, 故而递归查询栏目的子栏目id列表
        Set<Integer> set = new HashSet<Integer>();
        this.channelService.selectChildChannelIds(channel.getId(), set);
        if(set.contains(channel.getParentId())){
            req.setAttribute("msg", "该栏目下有子栏目或有内容");
            return TemplateUtil.adminError();
        }
        else{
            this.channelService.updateById(channel);
            return "redirect:/admin/channel";
        }
    }

    @RequestMapping(value = "/select-all", method = RequestMethod.GET)
    @ResponseBody
    public Result selectAll() {
        return new Result(this.channelService.selectChannelAll());
    }
}
