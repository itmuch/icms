package com.itmuch.icms.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class UrlUtil {

    public static String getContentUrl(Integer contentId) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getContextPath() + "/content/" + contentId;
    }

    public static String getChannelUrl(Integer channelId) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getContextPath() + "/channel/" + channelId;
    }
}
