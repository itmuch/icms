package com.itmuch.icms.util;

import org.springframework.ui.ModelMap;

public class TemplateUtil {
    /**
     * 跳转到成功页面
     * @param model
     * @param nextUrl
     * @return
     */
    public static String success(ModelMap model, String nextUrl) {
        model.put("nextUrl", nextUrl);
        return "default/common/success";
    }

    /**
     * 后台用的错误页面
     * @return 错误页面
     */
    public static String adminError() {
        return "admin/common/error";
    }
}
