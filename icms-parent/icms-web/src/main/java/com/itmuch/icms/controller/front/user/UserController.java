package com.itmuch.icms.controller.front.user;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.itmuch.core.constants.SessionConstants;
import com.itmuch.icms.user.domain.User;
import com.itmuch.icms.user.domain.UserInfo;
import com.itmuch.icms.user.service.IUserInfoService;
import com.itmuch.icms.user.service.IUserService;

@Controller
@RequestMapping("/u")
public class UserController {

    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IUserService userService;

    @RequestMapping("")
    public String index(HttpSession session, HttpServletRequest req) {
        Integer userId = (Integer) session.getAttribute(SessionConstants.USER_ID);
        UserInfo userInfo = this.userInfoService.selectByUserId(userId);
        User user = this.userService.selectById(userId);
        req.setAttribute("userInfo", userInfo);
        req.setAttribute("user", user);
        return "user/userIndex";
    }
}
