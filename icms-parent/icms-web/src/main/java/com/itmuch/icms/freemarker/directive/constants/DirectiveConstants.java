package com.itmuch.icms.freemarker.directive.constants;

public class DirectiveConstants {
    public static String ID = "id";
    public static String CHANNEL_LIST = "channels";
    public static String CONTENT_LIST = "contents";
    public static String CONTENT_LIST_PAGE = "pager";
}
