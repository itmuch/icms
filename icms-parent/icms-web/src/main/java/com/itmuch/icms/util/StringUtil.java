package com.itmuch.icms.util;

import com.itmuch.core.exception.ParamInvalidException;

public class StringUtil {
    public static Integer[] parseStringToIntegerArray(String param) {
        if (param != null) {
            String[] split = param.split(",");

            if ((split != null) && (split.length > 0)) {
                Integer[] ret = new Integer[split.length];
                for (int i = 0; i < split.length; i++) {
                    try {
                        ret[i] = Integer.parseInt(split[i]);
                    } catch (NumberFormatException e) {
                        throw new ParamInvalidException(new StringBuffer("参数").append(split[i]).append("无法转换成整型数字").toString());
                    }
                }
                return ret;
            }
        }
        return null;
    }
}
