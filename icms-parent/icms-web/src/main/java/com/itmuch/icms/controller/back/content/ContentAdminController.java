package com.itmuch.icms.controller.back.content;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.itmuch.core.web.converter.Result;
import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.service.IChannelService;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.content.vo.ContentAdminSearchVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/content")
public class ContentAdminController {
    @Resource
    private IContentService contentService;
    @Resource
    private IChannelService channelService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(HttpServletRequest req) {
        List<Channel> list = this.channelService.selectChannelAll();
        req.setAttribute("channelList", list);
        return "admin/content/content";
    }

    @RequestMapping(value = "/select-by-con", method = RequestMethod.GET)
    public String listPage(ContentAdminSearchVo vo) {
        return "admin/content/contentByCon";
    }

    @RequestMapping(value = "/select-by-con-ajax", method = RequestMethod.GET)
    @ResponseBody
    public PageList<Content> listByChannelId(ContentAdminSearchVo vo) {
        return (PageList<Content>) this.contentService.selectByConditionAdminPaged(vo,
                new PageBounds(vo.getPage(), vo.getLimit(), Order.formString(vo.getSort())));
    }

    /**
     * 通过id字符串, 删除内容
     * @param ids 多个id用1,2,3的形式分隔
     * @return 删除了多少条
     */
    @RequestMapping(value = "/del-by-ids", method = RequestMethod.DELETE)
    @ResponseBody
    public Result delByIds(String ids) {
        String[] idStringArray = ids.split(",");
        List<Integer> idList = null;
        if (idStringArray != null) {
            idList = new ArrayList<Integer>();
            for (String idString : idStringArray) {
                Integer id = Integer.parseInt(idString);
                idList.add(id);
            }
        }
        return new Result(this.contentService.deleteByIds(idList));
    }

}
