package com.itmuch.icms.controller.front.member;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itmuch.core.constants.SessionConstants;
import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.service.IChannelService;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.content.vo.ContentContributeVo;
import com.itmuch.icms.util.TemplateUtil;

@Controller
@RequestMapping("/member")
public class MemberController {
    @Resource
    private IChannelService channelService;
    @Resource
    private IContentService contentService;

    /**
     * 会员中心首页.
     * @return 会员中心首页
     */
    @RequestMapping(value = "")
    public String index() {
        return "default/member/index";
    }

    /**
     * 添加投稿页面
     * @param session 会话
     * @param req 请求
     * @return 添加投稿页面
     */
    @RequestMapping(value = "/contribute-add", method = RequestMethod.GET)
    public String addPage(HttpSession session, HttpServletRequest req) {
        Integer groupId = (Integer) session.getAttribute(SessionConstants.GROUP_ID);
        // 有权发帖的最子级栏目
        List<Channel> list = this.channelService.selectChannelsHasRightByGroupId(groupId);

        req.setAttribute("channelList", list);

        return "default/member/contribute_add";
    }

    /**
     * 添加投稿
     * @param vo 投稿的文章信息
     * @param session 会话
     * @param map 视图对象
     * @return 投稿成功页面
     */
    @RequestMapping(value = "/contribute-add", method = RequestMethod.POST)
    public String add(ContentContributeVo vo, HttpSession session, ModelMap map) {
        this.contentService.addContribute(vo, session);
        // 返回到通用的成功提示页面
        return TemplateUtil.success(map, vo.getNextUrl());
    }

    @RequestMapping(value = "/contribute-list-{pageNo}", method = RequestMethod.GET)
    public String list(@PathVariable Integer pageNo, HttpSession session, ModelMap map, HttpServletRequest request) {
        map.put("pageNo", pageNo);
        map.put("path", new StringBuffer(request.getContextPath() + "/member/contribute-list-"));
        // map.put(SessionConstants.USER_ID, session.getAttribute(SessionConstants.USER_ID));
        return "default/member/contribute_list";
    }

    /**
     * 返回修改投稿的页面.
     * @param id 稿件id
     * @param req 请求
     * @param session 会话
     * @return 修改稿件的页面
     */
    @RequestMapping(value = "/contribute-edit-{id}", method = RequestMethod.GET)
    public String editPage(@PathVariable Integer id, HttpServletRequest req, HttpSession session) {
        Integer userId = (Integer) session.getAttribute(SessionConstants.USER_ID);
        ContentContributeVo content = this.contentService.selectByIdAndUserId(id, userId);
        req.setAttribute("content", content);

        Integer groupId = (Integer) session.getAttribute(SessionConstants.GROUP_ID);
        // 有权发帖的最子级栏目
        List<Channel> list = this.channelService.selectChannelsHasRightByGroupId(groupId);

        req.setAttribute("channelList", list);

        return "default/member/contribute_edit";
    }

    /**
     * 修改投稿.
     * @param vo 投稿对象
     * @return 
     */
    @RequestMapping(value = "/contribute-edit", method = { RequestMethod.PUT, RequestMethod.POST })
    public String edit(ContentContributeVo vo, ModelMap map) {
        this.contentService.updateContribute(vo);
        return TemplateUtil.success(map, vo.getNextUrl());
    }
}
