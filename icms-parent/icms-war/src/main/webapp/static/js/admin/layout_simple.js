/**
 * 常用的二栏布局
 */
$(function(){
	$('body').layout({ 
		applyDefaultStyles: false,
		north:{
			size:80,spacing_open:0,closable:true,resizable:false,closable:false,scrollToBookmarkOnLoad:true,showOverflowOnHover:false
		},
		west:{
			size:200,spacing_open:2,closable:true,resizable:true 
		}
	});
});