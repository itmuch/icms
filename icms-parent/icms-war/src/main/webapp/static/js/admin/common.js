$(function(){
	// 导航栏
	$("#buttonset").buttonset();
});


/**
主题切换
*/
$(function() {
		$("#switcher").jui_theme_switch({
			stylesheet_link_id: "ui-theme",
			datasource_url: $('#ctx').val()+"/static/js/jui_theme_switch-master/json_data/dist/default.json"
		});
});
/**
 * 用于左边的菜单，点击隐藏，再次点击展开
 */
$(function(){
	$('#b-menu h3').click(function(){
		var nex = $(this).next();
		if (nex.is(":hidden")) {        //如果下个标签是隐藏的 
            nex.show();                 
        }
        else{
        	nex.hide();
        }
	});
});