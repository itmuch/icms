/**
 * 三栏布局，例如：http://localhost:8888/icms/admin/content/
 */
$(function(){
	$('body').layout({
		applyDefaultStyles: false,
		north:{
			size:80,spacing_open:0,closable:true,resizable:false,closable:false,scrollToBookmarkOnLoad:true,showOverflowOnHover:false
		},
		west:{
			size:200,spacing_open:2,closable:true,resizable:true 
		},
		west__size:.30,
		west__childOptions:{
			west__size:.40,
			west:{
				spacing_open:1,closable:true,resizable:true 
			}
		},
		center__maskContents:true
	});
});