<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册</title>
<jsp:include page="../common/resources.jsp"></jsp:include>
<script type="text/javascript">
// 点击验证码，验证码刷新
$(function(){
	$('#captchaImg').click(function(){
		$('#captchaImg').attr('src', '${ctx}/captcha.jpg?' + Math.floor(Math.random()*100) );
	});
})


// 以下校验表单，如果校验成功，执行函数，并提交。
$(function(){
	$('#form').validate({
		rules: {
			username:{required:true, minlength: 2},
			password:{required:true, minlength: 2},
			password1:{required:true, minlength: 2, equalTo: "#password"},
			email:{required:true, email: true},
			captcha:{required:true}
		},messages: {
			username:"最少2个字符",
			password: "最少2个字符",
			password1:{minlength:"最少2个字符", equalTo:"两次密码输入不一样"}
		},submitHandler: function(form) 
	    {      
			var username = $('#username').val();
			var password = $('#password').val();
			var password1 = $('#password1').val();
			var email = $('#email').val();
			var captcha = $('#captcha').val();
			$.ajax({
				url: '${ctx}/reg', 
				data:JSON.stringify({'username':username, 'password':password, 'password1':password1, 'email':email, 'captcha':captcha}),
				dataType:"json",
				type: "post", 
				contentType:"application/json;charset=utf-8", 
				success:function(data){
					if(data.errorCode == 0){
						window.location.href = '${ctx}/login';
						return;
					}else{
						alert(data.msg);
					}
				},
				error:function(){
					alert('err');
				}
			});
	    }
		
		
		
		
		
		
	});
});


/* $(function(){
	$('#sub').click(function(){
		var username = $('#username').val();
		var password = $('#password').val();
		var password1 = $('#password1').val();
		var email = $('#email').val();
		var captcha = $('#captcha').val();
		$.ajax({
			url: '${ctx}/reg', 
			data:JSON.stringify({'username':username, 'password':password, 'password1':password1, 'email':email, 'captcha':captcha}),
			dataType:"json",
			type: "post", 
			contentType:"application/json;charset=utf-8", 
			success:function(data){
				alert(JSON.stringify(data));
			}
		});
	});
}); */
</script>
</head>
<body>
	<form id="form" action="${ctx }/reg" method="post">
		<ul>
		<li>用户名：<input name="username" id="username"><label for="username"></label></li>
		<li>密码：<input type="password" name="password" id="password"><label for="password"></label></li>
		<li>确认密码：<input type="password" name="password1" id="password1"><label for="password1"></label></li>
		<li>邮箱：<input name="email" id="email"><label for="email"></label></li>
		<li>验证码：<input name="captcha" id="captcha"><label for="captcha"></label><img id="captchaImg" alt="验证码" src="${ctx }/captcha.jpg"></li>
		<li>提交：<input type="submit" value="提交" id="sub"></li>
		
		</ul>
	</form>
</body>
</html>