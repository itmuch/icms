<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册</title>
<jsp:include page="../common/resources.jsp"></jsp:include>
    <!-- 配置文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.all.js"></script>

<script type="text/javascript">
$(function(){
	$('#captcha').click(function(){
		$('#captcha').attr('src', '${ctx}/captcha.jpg?' + Math.floor(Math.random()*100) );
	});
});




$(function(){
	$('#sub').click(function(){
		var email = UE.getEditor('container').getContent();
		$('#email').val(email);
		$("#form").submit();
	});
});
</script>
<style type="text/css">
#captcha{
	cursor:pointer; 
}
</style>
</head>
<body>
	<!-- 实例化编辑器 -->
	<script type="text/javascript">
		var ue = UE.getEditor('container', {toolbars: [[
		                                                'fullscreen', 'source', '|', 'undo', 'redo', '|',
		                                                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
		                                                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
		                                                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
		                                                'directionalityltr', 'directionalityrtl', 'indent', '|',
		                                                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
		                                                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
		                                                'simpleupload', 'insertimage', 'emotion', 'insertvideo', 'music', 'attachment', 'insertcode', 'pagebreak', 'template', 'background', '|',
		                                                'horizontal', 'snapscreen', 'wordimage', '|',
		                                                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
		                                                'preview', 'searchreplace', 'help', 'drafts'
		                                            ]]});
	</script>


<form id="form" action="${ctx }/reg" method="post">
用户名：<input name="username"><br>
密码：<input name="password" type="password"><br>
邮箱：<input name="email" id="email"><br>
验证码：<input name="captcha"><img id="captcha" alt="验证码" src="${ctx }/captcha.jpg"><br>
想说的话：
 	<!-- 加载编辑器的容器 -->
    <script id="container" name="content" type="text/plain" style="width:600px;height:300px;"></script>




提交：<input type="button" value="提交" id="sub"> 
</form>
</body>
</html>