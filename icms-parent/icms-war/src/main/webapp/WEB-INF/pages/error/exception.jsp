<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>错误页面：${pageContext.response.status}</title>
<jsp:include page="../common/resources.jsp"></jsp:include>
<script type="text/javascript">
	$(function() {
		function hideDetail() {
			$('.detail p').hide();
			$('.detail p:lt(5)').show();
			$('#hideDetail').hide();
		}
		hideDetail();
		$('#showDetail').click(function() {
			$('.detail p').show();
			$('#showDetail').hide();
			$('#hideDetail').show();
		});
		$('#hideDetail').click(function() {
			hideDetail();
			$('#hideDetail').hide();
			$('#showDetail').show();
		});
	});
</script>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

body {
	background: #DAD9D8;
}

.container {
	width: 960px;
	margin: 0 auto;
	padding-top: 40px;
}

.box {
	background: #F5F5F5;
	border-radius: 10px;
	padding: 15px;
}

.box a {
	text-decoration: none;
	font-weight: bold;
	padding-bottom: 10px;
}

.status {
	font-size: 36px;
	padding: 0 0 10px 10px;
}

.title {
	font-weight: bold;
	height: 30px;
	line-height: 30px;
}
</style>
</head>
<body>
	<div class="container">
		<div class="status">${pageContext.response.status}:${exception.message}</div>
		<div class="box">
			<div class="detail">
				<div class="title">${exception}</div>
				<b>异常详情：</b>
				<c:forEach items="${exception.stackTrace}" var="item">
					<p>${item }</p>
				</c:forEach>
			</div>
			<a href="javascript:void(0)" id="showDetail">查看详情</a> <a
				href="javascript:void(0)" id="hideDetail">收起</a>
		</div>
	</div>
</body>
</html>