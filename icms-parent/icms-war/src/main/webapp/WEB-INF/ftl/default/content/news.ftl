<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<#include "../../common/resources.ftl">
<#include "../../common/resources_highlight.ftl">
<title>${content.title}</title>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<#include "../common/header.ftl">
			<div class="row clearfix">
				<div class="col-md-8 column">
					<h1>${content.title}</h1>
					<p>${txt!}</p>
				</div>
				<div class="col-md-4 column">
					TAGS 预留
				</div>
			</div>
		</div>
	</div>
</div>



	<!-- 代码高亮 -->
    <script type="text/javascript">
    	SyntaxHighlighter.highlight();
    </script>


</body>
</html>