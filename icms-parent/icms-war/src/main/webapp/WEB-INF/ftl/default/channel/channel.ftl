<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<#include "../../common/resources.ftl">
<title>${channel.name }</title>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<#include "../common/header.ftl">
			<div class="row clearfix">
				<div class="col-md-8 column">
					<@iContentListPage channelIds=channel.id orderBy=1 limit=10 containChild=1>
						<ul class="list-unstyled">
							<#list pager.data as d>
								<li>
									<span class="channelName"><a href="${d.channelUrl!}">[${d.channelName!}]</a></span>
									<a href="${d.url!}">${d.title!}</a>
									<span style="float:right;">
										<#if (d.issueTime)??>
											${(d.issueTime)?string("yyyy-MM-dd HH:mm:ss")}
										<#else>
											为空
										</#if>
									</span>
								</li>
							</#list>
							<#include "../../macro/pager.ftl" />
							<@pag pager.pageNo pager.totalPageNo path 10/>
						</ul>
					</@iContentListPage>
				</div>
				<div class="col-md-4 column">
					TAGS 预留
				</div>
			</div>
		</div>
	</div>
</div>







</body>
</html>