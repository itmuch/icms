<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
* {
	margin: 0;
	padding: 0;
	list-style-type: none;
	overflow: hidden;
}
a{text-decoration: none;}

.left {
	float: left;
}

.right {
	float: right;
}

.clear {
	clear: both;
}

.top {
	height: 25px;
	background: #E6E9ED;
}

.box {
	width: 960px;
	margin: 0 auto;
}

.top_nav_l li {
	float: left;
	line-height: 25px;
	height: 25px;
	padding: 0 5px;
}

.top_nav_r li {
	float: right;
	line-height: 25px;
	height: 25px;
	padding: 0 5px;
}

.top_mid {
	width: 960px;
	margin: 0 auto;
	height: 100px;
}

.logo,.logo a {
	width: 150px;
	height: 98%;
	float: left;
}

.logo a {
	background: #FFF
		url("http://att3.citysbs.com/no/hangzhou/2013/06/03/14/145709_dklcamik_13b1c7c3a37d3d689fb0139dd88faf4a.png")
		no-repeat 0 0;
}

.nav {
	width: 960px;
	margin: 0 auto;
	background: #1B7AC4;
	border-radius:6px; 
}
.nav ul{margin-left:7px; }
.nav li {
	float: left;
}

.nav a {
	height: 45px;
	line-height: 45px;
	display: block;
	padding: 0 10px;
	
	color: #FFF;
}
.nav .highlight{background: #1B6AAA;
}
</style>
</head>
<body>
	<div class="header">
		<!-- 头部导航 -->
		<div class="top">
			<div class="box">
				<div class="left">
					<ul class="top_nav_l">
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
					</ul>
				</div>
				<div class="right">
					<ul class="top_nav_r">
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<!-- 页头中间部分，logo等 -->
		<div class="top_mid">
			<div class="logo">
				<a href="#" targe="_blank"></a>
			</div>
		</div>
		<!-- 导航栏 -->
		<div class="nav">
			
<@iChannelList>
<ul>
	<#list channels as c>
	<li><a href="${ctx}/${c.path}">${c.name}</a></li>
	</#list>
</ul>
</@iChannelList>
			
		</div>
	</div>
	
	
	
	${content.title}
	${txt!}
	
</body>
</html>