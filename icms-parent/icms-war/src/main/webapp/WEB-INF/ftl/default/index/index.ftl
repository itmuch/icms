<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<#include "../../common/resources.ftl">
<title>首页</title>
<style>
body{
	background:#ddd;
}
</style>
</head>
<body>
<div class="container">
	<#include "../common/header.ftl">
	<div class="row clearfix">
		<div class="col-md-4 column">
			<div class="panel panel-default">
			<div class="panel-heading">栏目1</div>
				<div class="panel-body">
					<ul class="list-unstyled">
						<@iContentList channelIds="1,2" recommend=true orderBy=3 typeId=1 img=1 excludeIds="2,3" limit=10>
							<#list contents as d>
							<li>
								<span class="channelName"><a href="${d.channelUrl!}">[${d.channelName!}]</a></span>
								<a href="${d.url!}">${d.title!}</a>
							</li>
							</#list>
						</@iContentList>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4 column">
			<div class="panel panel-default">
			<div class="panel-heading">栏目2</div>
				<div class="panel-body">
					<ul class="list-unstyled">
						<@iContentList channelIds="1" limit=10 containChild=1>
							<#list contents as d>
							<li>
								<span class="channelName"><a href="${d.channelUrl!}">[${d.channelName!}]</a></span>
								<a href="${d.url!}">${d.title!}</a>
							</li>
							</#list>
						</@iContentList>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4 column">
			<div class="panel panel-default">
			<div class="panel-heading">栏目3</div>
				<div class="panel-body">
					<ul class="list-unstyled">
						<@iContentList channelIds="1,2" recommend=true orderBy=3 typeId=1 img=1 excludeIds="2,3" limit=10>
							<#list contents as d>
							<li>
								<span class="channelName"><a href="${d.channelUrl!}">[${d.channelName!}]</a></span>
								<a href="${d.url!}">${d.title!}</a>
							</li>
							</#list>
						</@iContentList>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>