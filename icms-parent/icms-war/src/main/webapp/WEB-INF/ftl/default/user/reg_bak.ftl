<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
* {
	margin: 0;
	padding: 0;
	list-style-type: none;
	overflow: hidden;
}
a{text-decoration: none;}

.left {
	float: left;
}

.right {
	float: right;
}

.clear {
	clear: both;
}

.top {
	height: 25px;
	background: #E6E9ED;
}

.box {
	width: 960px;
	margin: 0 auto;
}

.top_nav_l li {
	float: left;
	line-height: 25px;
	height: 25px;
	padding: 0 5px;
}

.top_nav_r li {
	float: right;
	line-height: 25px;
	height: 25px;
	padding: 0 5px;
}

.top_mid {
	width: 960px;
	margin: 0 auto;
	height: 100px;
}

.logo,.logo a {
	width: 150px;
	height: 98%;
	float: left;
}

.logo a {
	background: #FFF
		url("http://att3.citysbs.com/no/hangzhou/2013/06/03/14/145709_dklcamik_13b1c7c3a37d3d689fb0139dd88faf4a.png")
		no-repeat 0 0;
}

.nav {
	width: 960px;
	margin: 0 auto;
	background: #1B7AC4;
	border-radius:6px; 
}
.nav ul{margin-left:7px; }
.nav li {
	float: left;
}

.nav a {
	height: 45px;
	line-height: 45px;
	display: block;
	padding: 0 10px;
	
	color: #FFF;
}
.nav .highlight{background: #1B6AAA;
}
</style>
</head>
<body>
	<div class="header">
		<!-- 头部导航 -->
		<div class="top">
			<div class="box">
				<div class="left">
					<ul class="top_nav_l">
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
					</ul>
				</div>
				<div class="right">
					<ul class="top_nav_r">
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
						<li>111</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<!-- 页头中间部分，logo等 -->
		<div class="top_mid">
			<div class="logo">
				<a href="#" targe="_blank"></a>
			</div>
		</div>
		<!-- 导航栏 -->
		<div class="nav">
			
<@iChannelList>
<ul>
	<#list channels as c>
	<li><a href="${ctx}/channel/${c.id}">${c.name}</a></li>
	</#list>
</ul>
</@iChannelList>
			
		</div>
	</div>
	
	==========================
	
	<form id="form" action="${ctx }/reg" method="post">
		<ul>
		<li>用户名：<input name="username" id="username"><label for="username"></label></li>
		<li>密码：<input type="password" name="password" id="password"><label for="password"></label></li>
		<li>确认密码：<input type="password" name="password1" id="password1"><label for="password1"></label></li>
		<li>邮箱：<input name="email" id="email"><label for="email"></label></li>
		<li>验证码：<input name="captcha" id="captcha"><label for="captcha"></label><img id="captchaImg" alt="验证码" src="${ctx }/captcha.jpg"></li>
		<li>提交：<input type="submit" value="提交" id="sub"></li>
		
		</ul>
	</form>
	
<@iContentList channelIds="1,2" recommend=true orderBy=3 typeId=1 img=1 excludeIds="2,3" limit=100>
	<#list contents as d>
	<li><a href="${ctx}/${d.id!}">${d.title!}</a></li>
	</#list>
</@iContentList>
=============
<#-- 分页标签 -->
<#--
<@iContentListPage channelIds="1,2,3" recomend=true orderBy=1 typeId=1 img=1 excludeIds="2,3" limit=3>
	<#list pager.data as d>
	<li><a href="${ctx}/${d.id!}">${d.title!}</a></li>
	</#list>
	<#include "../../macro/pager.ftl" />
	<@pag pager.pageNo pager.totalPageNo "/icms/channel/1/" 10/>
</@iContentListPage>
	-->
==========================









	
</body>
</html>
<script type="text/javascript" src="${ctx }/static/js/jquery-1.11.1.js"></script>
<!-- jquery验证框架 -->
<script type="text/javascript" src="${ctx }/static/js/jquery-validation-1.13.1/dist/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx }/static/js/jquery-validation-1.13.1/src/localization/messages_zh.js"></script>
<script type="text/javascript">
// 点击验证码，验证码刷新
$(function(){
	$('#captchaImg').click(function(){
		$('#captchaImg').attr('src', '${ctx}/captcha?' + Math.floor(Math.random()*100) );
	});
})


// 以下校验表单，如果校验成功，执行函数，并提交。
$(function(){
	$('#form').validate({
		rules: {
			username:{required:true, minlength: 2},
			password:{required:true, minlength: 2},
			password1:{required:true, minlength: 2, equalTo: "#password"},
			email:{required:true, email: true},
			captcha:{required:true}
		},messages: {
			username:"最少2个字符",
			password: "最少2个字符",
			password1:{minlength:"最少2个字符", equalTo:"两次密码输入不一样"}
		},submitHandler: function(form) 
	    {      
			var username = $('#username').val();
			var password = $('#password').val();
			var password1 = $('#password1').val();
			var email = $('#email').val();
			var captcha = $('#captcha').val();
			$.ajax({
				url: '${ctx}/reg', 
				data:JSON.stringify({'username':username, 'password':password, 'password1':password1, 'email':email, 'captcha':captcha}),
				dataType:"json",
				type: "post", 
				contentType:"application/json;charset=utf-8", 
				success:function(data){
					if(data.errorCode == 0){
						window.location.href = '${ctx}/login';
						return;
					}else{
						alert(data.msg);
					}
				},
				error:function(){
					alert('err');
				}
			});
	    }
	});
});
</script>