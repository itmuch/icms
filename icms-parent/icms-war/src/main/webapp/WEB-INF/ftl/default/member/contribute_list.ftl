<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿</title> 
	<#include "../../common/resources.ftl">
    <!-- 配置文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.all.js"></script>
<style>
.b-wrapper{
	margin:0 auto;
}
.lp30 {
	width: 15%;
	float: left;
}

.lp65 {
	width: 80%;
	float: left;
	padding-left: 10px;
}
</style>
	<script type="text/javascript">
		// 点击验证码，验证码刷新
		$(function(){
			$('#captchaImg').click(function(){
				$('#captchaImg').attr('src', '${ctx}/captcha.jpg?' + Math.floor(Math.random()*100) );
			});
		});
	</script>
    <script>
    	// 以下校验表单，如果校验成功，执行函数，并提交。
        $(function(){
            UE.getEditor('content');

            var validator = $("#myform").submit(function() {
                UE.getEditor('content').sync();
            }).validate({
                        ignore: "",
                        rules: {
                            title: {required:true, minlength: 2},
                            content: "required"
                        },
                        errorPlacement: function(label, element) {
                            label.insertAfter(element.is("textarea") ? element.next() : element);
                        }
                    });
            validator.focusInvalid = function() {
                if( this.settings.focusInvalid ) {
                    try {
                        var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                        if (toFocus.is("textarea")) {
                            UE.getEditor('content').focus()
                        } else {
                            toFocus.filter(":visible").focus();
                        }
                    } catch(e) {
                    }
                }
            }
        })
    </script>




</head>
<body>
<div class="container">
	<#include "../common/header.ftl">
    <script type="text/javascript">
		var ue = UE.getEditor('content', {toolbars: [['fullscreen', 'source', '|', 'undo', 'redo', '|',
		                                                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
		                                                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
		                                                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
		                                                'directionalityltr', 'directionalityrtl', 'indent', '|',
		                                                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
		                                                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
		                                                'simpleupload', 'insertimage', 'emotion', 'insertvideo', 'music', 'attachment', 'insertcode', 'pagebreak', 'template', 'background', '|',
		                                                'horizontal', 'snapscreen', 'wordimage', '|',
		                                                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
		                                                'preview', 'searchreplace', 'help', 'drafts'
		                                            ]]});
	</script>
		<div class="b-wrapper">
			<div class="lp30">
				<div class="row clearfix">
					<div class="col-md-12 column">
						<div class="list-group">
							<a href="${ctx}/member" class="list-group-item active">会员中心首页</a>
							<a href="${ctx}/member/contribute-add" class="list-group-item">投稿</a>
							<a href="${ctx}/member/contribute-list-1" class="list-group-item">查看</a>
					</div>
				</div>
			</div>
		</div>
		<#-- 右侧 -->
		<div class="lp65">
			<@iUserContentListPage limit=10>
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th>ID</th>
							<th>栏目</th>
							<th>标题</th>
							<th>操作</th>
						</tr>
					</thead>
					<#list pager.data as d>
						<tr>
							<td>${d.id}</td>
							<td><a href="${d.channelUrl!}">【${d.channelName}】</a></td>
							<td><a href="${d.url!}">${d.title!}</a></td>
							<td><a href="${ctx}/member/contribute-edit-${d.id}">编辑</td>
						</tr>
					</#list>
				</table>
				<#include "../../macro/pager.ftl" />
				<@pag pager.pageNo pager.totalPageNo pager.path 10/>
			</@iUserContentListPage>
		</div>
	</div>
</div>
</body>
</html>