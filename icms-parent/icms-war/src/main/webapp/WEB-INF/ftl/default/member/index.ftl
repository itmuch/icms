<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>个人中心-首页</title>
<#include "../../common/resources.ftl">
<style>
.b-wrapper{
	margin:0 auto;
}
.lp30 {
	width: 15%;
	float: left;
}

.lp65 {
	width: 80%;
	float: left;
	margin-left: 10px;
	padding:10px;
	border:solid 1px #ddd;
}
</style>
</head>
<body>
<div class="container">
	<#include "../common/header.ftl">
	<div class="b-wrapper">
			<div class="lp30">
				<div class="row clearfix">
					<div class="col-md-12 column">
						<div class="list-group">
							<a href="${ctx}/member" class="list-group-item active">会员中心首页</a>
							<a href="${ctx}/member/contribute-add" class="list-group-item">投稿</a>
							<a href="${ctx}/member/contribute-list-1" class="list-group-item">查看</a>
					</div>
				</div>
			</div>
		</div>
		<#-- 右侧 -->
		<div class="lp65">
			<p>欢迎${username!}回来！</p>
		</div>
	</div>
</div>
</body>
</html>