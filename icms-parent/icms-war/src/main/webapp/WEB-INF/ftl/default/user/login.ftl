<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<#include "../../common/resources.ftl">
<title>注册</title>
<style>
body{
	background:#ddd;
}
.bg-fff{
	background:#fff;
}
.regTable input{
	height:35px;
}
</style>
</head>
<body>
<div class="container">
	<#include "../common/header.ftl">
	<div class="row clearfix">
		<div class="col-md-6 column">
			<form role="form" id="form" action="${ctx }/login" method="post">
				<table class="regTable">
				<tr>
					<td>用 户 名：</td>
					<td><input name="username" id="username"><label for="username"></label></td>
				</tr>
				<tr>
					<td>密码：</td>
					<td><input type="password" name="password" id="password"><label for="password"></label></td>
				</tr>
				<tr>
					<td>提交：</td>
					<td><input type="submit" value="提交" id="sub"></td>
				</tr>
				</table>
			</form>
		</div>
		<div class="col-md-6 column">
			
		</div>
	</div>
</div>
</body>
</html>