	<#--<div class="row clearfix">
		<div class="col-md-12 column">-->
			<nav class="navbar navbar-default" role="navigation">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<@iChannelList>
					<ul class="nav navbar-nav">
						<#-- 这边判断比较简单，认为如果没有栏目，那就首页高亮；否则按照栏目id高亮 -->
						<li <#if !channel??>class="active"<#else></#if>><a href="${ctx}/">首页</a></li></li>
						<#list channels as c>
						<li <#if channel?? && channel.id == c.id>class="active"<#else></#if>>
						<a href="${c.url}">${c.name}</a></li>
						</#list>
					</ul>
					</@iChannelList>
					
					<form class="navbar-form navbar-left" role="search" id="993526384">
						<div class="form-group">
							<input type="text" class="form-control" />
						</div> <button type="submit" class="btn btn-default">搜索</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<#if (username)??>
							<li><a href="${ctx}/member">${username}</a></li>
							<li><a href="${ctx}/logout">退出</a></li>
						<#else>
							<li><a href="${ctx}/reg">注册</a></li>
							<li><a href="${ctx}/login">登陆</a></li>
						</#if>
						
					</ul>
				</div>
			</nav>
		<#--</div>
	</div>-->