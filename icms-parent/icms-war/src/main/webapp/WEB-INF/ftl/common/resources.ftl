<script type="text/javascript" src="${ctx }/static/js/jquery-1.11.1.js"></script>
<#-- jquery验证框架 -->
<script type="text/javascript" src="${ctx }/static/js/jquery-validation-1.13.1/dist/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx }/static/js/jquery-validation-1.13.1/src/localization/messages_zh.js"></script>
<#-- 整合bootstarp -->
<script type="text/javascript" src="${ctx }/static/js/bootstarp-3.3.1/js/bootstrap.js"></script>
<link href="${ctx }/static/js/bootstarp-3.3.1/css/bootstrap.css" rel="stylesheet">
