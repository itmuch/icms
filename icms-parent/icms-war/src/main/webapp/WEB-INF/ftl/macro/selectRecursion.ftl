<#macro selectRecursion data topId indent msg name class="" id="" selectId=-1>
	<select <#if name!="">name=${name}</#if> <#if class!="">class=${class}</#if> <#if id!="">id=${id}</#if>>
		<option value="0">${msg}</option>
		<#import "child.ftl" as my/>
		<@my.selectTree data=data topId=topId indent=indent selectId=selectId/>
	</select>
</#macro>

<#-- 
	使用方法：
	<@selectRecursion data=channelList id=0 indent=1 msg="请选择"/>
	示例：
	data为准备好的list，id指最顶级元素的parent_id，indent为缩进，0表示无缩进，msg你懂的
 -->