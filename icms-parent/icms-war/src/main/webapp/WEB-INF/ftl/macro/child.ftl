<#macro selectTree data topId indent selectId=-1>
	<#local temp = indent+1 />
		<#list data as item>
			<#if topId == item.parentId>
				<#if selectId == item.id>
					<option value="${item.id}" selected><#if temp gte 2><#list 1..temp as x><#if x gt 2>&nbsp;&nbsp;&nbsp;&nbsp;</#if></#list>&nbsp;&nbsp;&nbsp;&nbsp;└- </#if>${item.name!}</option>
				<#else>
					<option value="${item.id}"><#if temp gte 2><#list 1..temp as x><#if x gt 2>&nbsp;&nbsp;&nbsp;&nbsp;</#if></#list>&nbsp;&nbsp;&nbsp;&nbsp;└- </#if>${item.name!}</option>
				</#if>
				<@selectTree data=data topId=item.id indent=temp selectId=selectId/>
				<#else>
			</#if>
		</#list>
</#macro>

<#-- 
 使用方法：<@selectTree data=data topId=顶级元素id indent=indent />
 其中：data是想要取的list id是顶级元素的parent_id，indent是缩进偏移，0表示无缩进
 -->