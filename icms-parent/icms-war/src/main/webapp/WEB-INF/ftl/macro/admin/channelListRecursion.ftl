<#macro cList data topId indent>
	<#local temp = indent+1 />
					<#list data as item>
						<#if topId == item.parentId>
							<tr>
								<td><input style="width:40px;height:20px;" name="priority" value="${item.priority?c}"/></td>
								<td>${item.id}<input type="hidden" name="ids" value="${item.id?c}" /></td>
								<td><#if temp gte 2><#list 1..temp as x><#if x gt 2>&nbsp;&nbsp;&nbsp;&nbsp;</#if></#list>&nbsp;&nbsp;&nbsp;&nbsp;└- </#if>${item.name!}</td>
								<td><a href="${item.url!}" target="_blank">访问</a></td>
								<td>${(item.displayFlag==1)!?string('是','否')}</td>
								<td>
									<a class="label bg-blue" href="${ctx}/admin/channel/add/${item.id}">添加子栏目</a>
									<a class="label bg-yellow" href="${ctx}/admin/channel/edit/${item.id}">修改</a>
									<a class="label bg-red" href="${ctx}/admin/channel/del-by-id/${item.id}">删除</a>
								</td>
							</tr>
							<@cList data=data topId=item.id indent=temp/>
						<#else>
						</#if>
					</#list>
</#macro>
<#-- 用于后台递归生成栏目列表 -->