<#macro cTable data topId indent=0>

<table class="table table-striped">
    <tr>
        <th>排序</th>
        <th>id</th>
        <th>栏目名称</th>
        <th>访问</th>
        <th>是否可见</th>
        <th>操作</th>
    </tr>
    <#include "channelListRecursion.ftl">
    <@cList channelList topId indent/>

</table>
</#macro>
<#-- 用于后台递归生成栏目列表，直接生成table -->