<#-- 分页控件-->
<#--
	pageNoV:当前页码
	totalPageNoV：总共多少页
	maxShowPagesV:下面的页码最多加载多少页

-->
<#macro pag pageNoV totalPageNoV path maxShowPagesV=5 >
	<#-- 本次分页起始页 -->
	<#local pageStartNo = 1/>
	<#-- 本次分页结束页 -->
	<#local pageEndNo = maxShowPagesV />
	<#-- 第一页 -->
	<#local firstPageNo = 1 />
	<#-- 最后一页 -->
	<#local lastPageNo = totalPageNoV />
	<#-- 前一页号码 -->
	<#local prePageNo = pageNoV - 1 />
	<#-- 后一页号码 -->
	<#local nextPageNo = pageNoV + 1 />
	<#-- 当前所在的循环 -->
	<#local segment = 0/>
	
	<#--如果是第一页-->
	<#if pageNoV == 1>
		<#local prePageNo = 1 />
	</#if>
	<#-- 如果是最后一页 -->
	<#if pageNoV == totalPageNoV>
		<#local nextPageNo = totalPageNoV />
	</#if>
	<#--如果总页码 大于 一次加载的页码-->
	<#if totalPageNoV gt maxShowPagesV>
		<#-- 如果传入的当前页码<总页码  -->
		<#if pageNoV <= totalPageNoV>
			<#if pageNoV % maxShowPagesV != 0>
				<#-- (pageNoV/maxShowPagesV)?int  例如：13/10+1 表明在第二个循环 -->
				<#local segment = (pageNoV/maxShowPagesV)?int />
			<#else>
				<#-- 例如第10页，要显示的应该是1-5页，是在第一个循环 -->
				<#local segment = (pageNoV/maxShowPagesV)?int-1 />
			</#if>
			<#-- 例如传的12 那就是1*10+1 -->
			<#local pageStartNo = segment*maxShowPagesV +1 />
			<#-- 例如传的12，最后一页就是20 -->
			<#local pageEndNo = (segment+1)*maxShowPagesV />
			<#if pageEndNo gt totalPageNoV>
				<#local pageEndNo = totalPageNoV />
			<#else>
			</#if>
		<#-- 如果传入的当前页码>总页码，那么只展示最后一个循环，当前页码设为最后一页 -->
		<#else>
			<#local pageNoV = totalPageNoV />
			<#local pageStartNo = totalPageNoV-maxShowPagesV+1 />
			<#local pageEndNo = totalPageNoV />
			<#local prePageNo = totalPageNoV-1 />
			<#local nextPageNo = totalPageNoV />
		</#if>
		
	<#--总页码小于一次加载的页码时，直接显示所有页码-->
	<#else>
		<#local pageEndNo = totalPageNoV/>
	</#if>
	
	<#-- 开始生成页面，第一页有个多入口的问题，有空可以解决一下，例如：/channel/1 但是分页后是/channel/1/1 -->
	<ul class="pagination">
		<#if pageNoV gt 1>
			<li><a href="${path}${prePageNo}">&laquo;</a></li>
		<#else>
		</#if>
		
		<#list pageStartNo..pageEndNo as x>
			<li><a href="${path}${x}">${x}</a></li>
		</#list>
		
		<#if pageNoV lt totalPageNoV>
			<li><a href="${path}${nextPageNo}">&raquo;</a></li>
		<#else>
		</#if>
		<li><a>第${pageNoV}页/共${totalPageNoV}页</a></li>
	</ul>
	

	
	
</#macro>