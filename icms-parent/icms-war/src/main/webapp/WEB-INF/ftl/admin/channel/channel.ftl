<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>后台管理</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<#include "../../resources/resources-admin-lte-css.ftl">

</head>
<body class="skin-blue-light sidebar-mini">
<div class="wrapper">
<#include "../common/header.ftl">
<#include "../common/aside.ftl">
    <!-- 中央部分 -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                栏目
                <small>管理</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
                <li><a href="#">栏目管理</a></li>
                <li class="active">栏目列表</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">栏目列表</h3>
                        </div>
                        <form method="post" action="${ctx}/admin/channel/edit-priority">
                            <div class="box-body no-padding">
                            <#include "../../macro/admin/channelListTable.ftl">
                            <@cTable channelList 0 0 />
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-default"><i class="fa fa-pencil"></i> 排序</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
<#include "../common/footer.ftl">
</div>

<#include "../../resources/resources-admin-lte-js.ftl">
</body>
</html>