<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>后台管理</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

<#include "../../resources/resources-admin-lte-css.ftl">
</head>
<body class="skin-blue-light sidebar-mini">
<div class="wrapper">
<#include "../common/header.ftl">
<#include "../common/aside.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                栏目
                <small>管理</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
                <li><a href="#">栏目管理</a></li>
                <li class="active">添加栏目</li>
            </ol>
        </section>
        <!-- 中央部分 -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">


                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">基本选项</a></li>
                            <li><a href="#tab_2" data-toggle="tab">SEO</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">


                                <form action="${ctx}/admin/channel/add" method="post">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="parentId">上级栏目</label>
                                        <#include "../../macro/selectRecursion.ftl">
                                        <@selectRecursion data=channelList topId=0 indent=0 msg="--请选择--" class="form-control" name="parentId" id="parentId" selectId=parentId />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">栏目名称</label>
                                            <input class="form-control" name="name" type="text"/>
                                        </div>
                                        <input type="submit"/>
                                    </div>
                                    <!-- /.box-body -->
                                </form>


                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                The European languages are members of the same family. Their separate existence is a
                                myth.
                                For science, music, sport, etc, Europe uses the same vocabulary. The languages only
                                differ
                                in their grammar, their pronunciation and their most common words. Everyone realizes why
                                a
                                new common language would be desirable: one could refuse to pay expensive translators.
                                To
                                achieve this, it would be necessary to have uniform grammar, pronunciation and more
                                common
                                words. If several languages coalesce, the grammar of the resulting language is more
                                simple
                                and regular than that of the individual languages.
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->


                </div>
            </div>
        </section>
    </div>
<#include "../common/footer.ftl">
</div>
<#include "../../resources/resources-admin-lte-js.ftl">
</body>
</html>