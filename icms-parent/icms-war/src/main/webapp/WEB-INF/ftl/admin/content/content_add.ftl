<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>后台管理</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- 配置文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.all.js"></script>
<#include "../../resources/resources-admin-lte-css.ftl">

</head>
<body class="skin-blue-light sidebar-mini">
<script type="text/javascript">
    var ue = UE.getEditor('txt', {
        toolbars: [['fullscreen', 'source', '|', 'undo', 'redo', '|',
            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
            'directionalityltr', 'directionalityrtl', 'indent', '|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
            'simpleupload', 'insertimage', 'emotion', 'insertvideo', 'music', 'attachment', 'insertcode', 'pagebreak', 'template', 'background', '|',
            'horizontal', 'snapscreen', 'wordimage', '|',
            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
            'preview', 'searchreplace', 'help', 'drafts'
        ]]
    });
</script>

<div class="wrapper">
<#include "../common/header.ftl">
<#include "../common/aside.ftl">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                发布
                <small>内容</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
                <li><a href="#">内容管理</a></li>
                <li class="active">发布</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">发布</h3>
                        </div>
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">标题</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="title" class="form-control" id="title"
                                               placeholder="请输入标题">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="author" class="col-sm-2 control-label">作者</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="author" class="form-control" id="author"
                                               placeholder="请输入作者">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="summary" class="col-sm-2 control-label">摘要</label>

                                    <div class="col-sm-10">
                                        <textarea rows="3" name="summary" class="form-control"
                                                  id="summary"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="txt" class="col-sm-2 control-label">内容</label>

                                    <div class="col-sm-10">
                                        <textarea name="txt" id="txt"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info">提交</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
<#include "../common/footer.ftl">
</div>
<#include "../../resources/resources-admin-lte-js.ftl">
</body>
</html>