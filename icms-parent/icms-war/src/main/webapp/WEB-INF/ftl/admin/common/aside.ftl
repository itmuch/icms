<aside class="main-sidebar">
    <!-- 左侧 -->
    <section class="sidebar">
        <!-- 搜索栏 -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- 左侧菜单 -->
        <ul class="sidebar-menu">
            <li class="header">导航</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i> <span>内容管理</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="${ctx}/admin/channel">
                            <i class="fa fa-circle-o"></i>
                            栏目管理
                        </a>
                    </li>
                    <li>
                        <a href="${ctx}/admin/content">
                            <i class="fa fa-circle-o"></i>
                            内容管理
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>