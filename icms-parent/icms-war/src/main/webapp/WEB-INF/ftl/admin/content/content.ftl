<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>后台管理</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- 配置文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="${ctx }/static/js/ueditor1_4_3-utf8-jsp/ueditor.all.js"></script>
<#include "../../resources/resources-admin-lte-css.ftl">
</head>
<body class="skin-blue-light sidebar-mini">
<div class="wrapper">
<#include "../common/header.ftl">
<#include "../common/aside.ftl">
    <!-- 中央部分 -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                内容
                <small>管理</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
                <li><a href="#">内容管理</a></li>
                <li class="active">内容列表</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">内容列表</h3>
                        </div>
                        <div class="box-body no-padding">
                        <#--tree-->
                            <div class="col-md-3">
                                <ul id="contentTree" class="ztree"></ul>
                            </div>
                        <#--内容列表-->
                            <div class="col-md-9">
                                <div class="box">
                                    <div class="box-header">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <input type="button" id="btnRemoveSelected" class="btn btn-danger"
                                                       value="删除所选"/>
                                                <input class="btn btn-info" type="button" data-toggle="modal"
                                                       data-target="#modalId" value="添加内容">
                                                <input class="form-control" type="text" name="title" id="title"
                                                       placeholder="Search for...">
                                                <button class="btn btn-info" type="button" id="searchBtn">Go!</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="box-body">
                                        <table id="table11-1"></table>
                                        <div id="paginator11-1"></div>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" id="channelId"/>
                            <input type="hidden" id="ctx" value="${ctx}"/>
                        </div>

                    <#--添加-->
                        <div class="modal fade in" id="modalId">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">添加</h4>
                                    </div>
                                    <form class="form-horizontal" action="">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="title" class="col-sm-2 control-label">栏目</label>

                                                <div class="col-sm-10">
                                                <#include "../../macro/selectRecursion.ftl">
                                                 <@selectRecursion data=channelList topId=0 indent=0 msg="--请选择--" class="form-control" name="parentId" id="parentId" selectId=0 />

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="title" class="col-sm-2 control-label">标题</label>

                                                <div class="col-sm-10">
                                                    <input type="text" name="title" class="form-control" id="title"
                                                           placeholder="请输入标题">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="author" class="col-sm-2 control-label">作者</label>

                                                <div class="col-sm-10">
                                                    <input type="text" name="author" class="form-control" id="author"
                                                           placeholder="请输入作者">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="summary" class="col-sm-2 control-label">摘要</label>

                                                <div class="col-sm-10">
                                                    <textarea rows="3" name="summary" class="form-control"
                                                              id="summary"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="txt" class="col-sm-2 control-label">内容</label>

                                                <div class="col-sm-10">
                                                    <textarea name="txt" id="txt" style="width:100%;"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">
                                                取消
                                            </button>
                                            <button type="submit" class="btn btn-primary">确定</button>
                                        </div>
                                    </form>
                                </div>
                            <#-- /.modal-content -->
                            </div>
                        <#-- /.modal-dialog -->
                        </div>
                    <#--/.modal -->
                    </div>
                <#--/.box-->
                </div>
            <#--.col-xs-12-->
            </div>
        <#--.row-->
        </section>
    </div>
<#include "../common/footer.ftl">
</div>
<#include "../../resources/resources-admin-lte-js.ftl">
<#include "../../common/resources_mmgrid.ftl">
<#include "../../common/resources_format_date.ftl">
<link rel="stylesheet" href="${ctx}/static/js/mmGrid/theme/bootstrap/mmGrid-bootstrap.css">
<link rel="stylesheet" href="${ctx}/static/js/mmGrid/theme/bootstrap/mmPaginator-bootstrap.css">
<#include "../../common/resources_ztree.ftl">
<script type="text/javascript">
    $(function () {

    });
</script>
<script type="text/javascript">
    // 初始化列表
    $(function () {
        //分页
        var mmg;
        // 初始化树形
        $.ajax({
            url: '${ctx}/admin/channel/select-all',
            dataType: "json",
            type: "get",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                var setting = {
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id",
                            pIdKey: "parentId"
                        }
                    },
                    callback: {
                        beforeClick: function (treeId, treeNode) {
                            var zTree = $.fn.zTree.getZTreeObj("contentTree");
                            // 如果是父节点, 单击展开父节点
                            if (treeNode.isParent) {
                                zTree.expandNode(treeNode);
                                return false;
                            } else {
                                $('#channelId').val(treeNode.id);
                                mmg.load({page: 1});
                                return true;
                            }
                        }
                    }
                };
                var jsonData = data.data;
                var treeObj = $.fn.zTree.init($("#contentTree"), setting, jsonData);
                treeObj.expandAll(true);
            },
            error: function () {
                alert('err');
            }
        });

        var cols = [{title: 'id', name: 'id', width: 40, align: 'left', sortable: true, type: 'number'},
            {title: '标题', name: 'title', width: 100, align: 'left'},
            {title: '内容类型', name: 'typeId', width: 100, align: 'left'},
            {
                title: '发表时间', name: 'issueTime', width: 200, align: 'left', renderer: function (val) {
                return $.formatDate("yyyy-MM-dd HH:mm:ss", new Date(val));
            }
            }];

        mmg = $('#table11-1').mmGrid({
            height: 'auto',
            remoteSort: true,
            sortName: 'id',
            sortStatus: 'DESC',
            cols: cols,
            multiSelect: true,
            fullWidthRows: true,
            checkCol: true,
            url: '${ctx}/admin/content/select-by-con-ajax',
            root: 'items',
            method: 'get',
            plugins: [$('#paginator11-1').mmPaginator({limitList: [10, 20, 30, 50]})],
            params: function () {
                var title = $('#title').val();
                var channelId = $('#channelId').val();

                var obj = new Object();
                obj.channelId = channelId;
                // 输入了条件了
                if (title != null && title != '') {
                    obj.title = title;
                }
                return obj;
            }
        });

        // 为搜索按钮添加事件
        $('#searchBtn').click(function () {
            mmg.load({page: 1});
        });
        // 为删除所选按钮添加事件
        $('#btnRemoveSelected').on('click', function () {
            var selectedRows = mmg.selectedRows();
            var selectedIndexes = mmg.selectedRowsIndex();
            var ids = '';
            $.each(selectedRows, function (i) {
                var jsonItem = selectedRows[i];
                var id = jsonItem.id;
                ids += id + ',';
            });
            if (ids != '' && ids.length > 0) {
                if (confirm('你确定要删除该行记录吗?')) {
                    $.ajax({
                        type: "DELETE",
                        url: '${ctx}/admin/content/del-by-ids?ids=' + ids.substring(0, ids.length - 1),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            // 删除成功后，页面上删除勾选了的行
                            mmg.removeRow(selectedIndexes);
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                }
            } else {
                alert('未勾选任何内容');
            }
        });

        // 为添加内容按钮添加事件
        $('#btnAdd').click(function () {
            var channelId = $('#channelId').val();
            var href = '${ctx}/admin/content/add/' + channelId;
            location.href = href;
        });
        // 结束
    });
</script>
<script type="text/javascript">
    var ue = UE.getEditor('txt', {
        toolbars: [['fullscreen', 'source', '|', 'undo', 'redo', '|',
            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
            'directionalityltr', 'directionalityrtl', 'indent', '|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
            'simpleupload', 'insertimage', 'emotion', 'insertvideo', 'music', 'attachment', 'insertcode', 'pagebreak', 'template', 'background', '|',
            'horizontal', 'snapscreen', 'wordimage', '|',
            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
            'preview', 'searchreplace', 'help', 'drafts'
        ]]
    });
</script>
</body>
</html>