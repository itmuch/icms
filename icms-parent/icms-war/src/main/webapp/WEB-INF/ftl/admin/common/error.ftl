<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>错误提示页</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

<#include "../../resources/resources-admin-lte-css.ftl">
    <style>
        body {
            background-color: Transparent;
        }
    </style>
</head>
<body>
<section class="content-header">
    <h1>
        错误
        <small>页</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">错误</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">错误详情</h3>
        </div>
        <div class="box-body">
            操作有误：${msg}<br>
        </div>
        <div class="box-footer">
            <a class="label bg-blue" href="javascript:history.back();">点击这里返回上一页</a>
        </div>
    </div>

</section>

<#include "../../resources/resources-admin-lte-js.ftl">
</body>
</html>