<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
<link rel="stylesheet" href="${ctx }/static/js/mmGrid/mmGrid.css">
<link rel="stylesheet" href="${ctx }/static/js/mmGrid/mmPaginator.css">

<script type="text/javascript" src="${ctx }/static/js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="${ctx }/static/js/mmGrid/mmGrid.js"></script>
<script type="text/javascript"
    src="${ctx }/static/js/mmGrid/mmPaginator.js"></script>
<script type="text/javascript">
	$(function() {
		var cols = [ {
			title : 'id',
			name : 'id',
			width : 50,
			align : 'center',
			// 排序
			sortable: true,
			type: 'number'
		}, {
			title : '名称',
			name : 'name',
			width : 100,
			align : 'center'
		}, {
			title : '父id',
			name : 'parentId',
			width : 100,
			align : 'center'
		},{
			title : '状态',
			name : 'delFlag',
			width : 100, 
			align: 'center',
			renderer:function(val){
				// 当屏显与数值不同时，可使用
				if(val == 0){
					return '正常';
				}else{
					return '已删除';
				}
			}
			
		},{
			title: '操作',
			name:'',
			width:150,
			align:'center',
			lockWidth:true,
			lockDisplay: true,
			renderer: function(val){
                return '<button  class="btn btn-info">查看</button> <button  class="btn btn-danger">删除</button>'
            }
		} ];
		
		//分页
		var mmg = $('#table11-1').mmGrid({
			// 索引列
			indexCol : true,
			indexColWidth : 25,
			
			// 排序
			// 服务器端排序，如果不写则是客户端排序
			// remoteSort:true ,
			sortName: 'id',
	        sortStatus: 'DESC',
	        
			// 可以一次选中多个
			multiSelect : true,
			
			// 勾选项
			checkCol : true,
			indexColWidth : 35,
			cols : cols,
			url : '${ctx}/list',
			method : 'get',
			root : 'items',
			plugins : [ $('#paginator11-1').mmPaginator() ],
			// 条件查询，将参数放在这里
			params: function(){
				var nameVal = $('#name').val();
				// 如果没有输入条件
				if(nameVal == null|| nameVal == ''){
					return {}
				}
				// 输入了条件了
				else{
             		 return {name: nameVal}
				}
           }
		});
		
		// 操作选中项
		mmg.on('cellSelected', function(e, item, rowIndex, colIndex){
            console.log('cellSelected!');
            console.log(this);
            console.log(e);
            console.log(item);
            console.log(rowIndex);
            console.log(colIndex);
            //查看
            if($(e.target).is('.btn-info, .btnPrice')){
                e.stopPropagation();  //阻止事件冒泡
                alert(JSON.stringify(item));
            }
            // 删除
            else if($(e.target).is('.btn-danger') && confirm('你确定要删除该行记录吗?')){
                e.stopPropagation(); //阻止事件冒泡
                var id = item.id;
                $.ajax({
        			type : "DELETE",
        			url : '${ctx}/del-by-id/'+id,
        			// data : data,
        			contentType : "application/json; charset=utf-8",
        			dataType : "json",
        			success : function(data) {
        				// 删除成功后，刷新当前行
        				item.delFlag = '已删除';
        				mmg.updateRow(item, rowIndex);
        			}
                });
                
            }
		});
		// 操作选中项结束
		
		$('#searchBtn').click(function(){
			mmg.load({page:1});
		});
		
	});
	
</script>
<style>
.grid-100 {
	width: 800px;
}

.mmGrid,.mmPaginator {
	font-size: 12px;
}

.btnPrice {
	display: block;
	width: 16px;
	height: 16px;
	margin: 0px auto;
	background: url(img/botton_g1.gif) no-repeat;
}

.btnPrice:hover {
	background: url(img/botton_g2.gif) no-repeat;
}
</style>
</head>
<body>
    <div>
        <input type="text" value="" name="name" id="name">
        <button id="searchBtn">搜索</button>
    </div>
    <div class="grid-100">
        <table id="table11-1"></table>
        <div style="text-align: right;">
            <div id="paginator11-1"></div>
        </div>
    </div>

</body>
</html>