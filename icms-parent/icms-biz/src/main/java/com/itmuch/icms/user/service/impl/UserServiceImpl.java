package com.itmuch.icms.user.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.user.domain.User;
import com.itmuch.icms.user.persistence.UserMapper;
import com.itmuch.icms.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public int deleteByIdReal(Integer id) {
        return this.userMapper.deleteByIdReal(id);
    }

    @Override
    public int deleteById(Integer id) {
        return this.userMapper.deleteById(id);
    }

    @Override
    public int insert(User record) {
        return this.userMapper.insert(record);
    }

    @Override
    public User selectById(Integer id) {
        return this.userMapper.selectById(id);
    }

    @Override
    public int updateById(User record) {
        return this.userMapper.updateById(record);
    }

    @Override
    public User selectByUsername(String username) {
        return this.userMapper.selectByUsername(username);
    }

}
