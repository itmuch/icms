package com.itmuch.icms.content.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ContentAdminSearchVo {
    /**
     * id
     */
    private Integer id;
    /**
     * 栏目id
     */
    private Integer channelId;
    /**
     * 内容类型 0普通 1图文 2焦点 3头条
     */
    private Byte typeId;
    /**
     * 标题
     */
    private String title;
    /**
     * 推荐标识
     */
    private Byte recomendFlag;
    /**
     * 状态
     */
    private Byte status;
    /**
     * 用户名
     */
    private String username;
    /**
     * 起始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /**
     * 当前页码
     */
    private Integer page = 1;
    /**
     * 单页显示多少条
     */
    private Integer limit = 10;

    private String sort = "id.DESC";

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChannelId() {
        return this.channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Byte getTypeId() {
        return this.typeId;
    }

    public void setTypeId(Byte typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Byte getRecomendFlag() {
        return this.recomendFlag;
    }

    public void setRecomendFlag(Byte recomendFlag) {
        this.recomendFlag = recomendFlag;
    }

    public Byte getStatus() {
        return this.status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getPage() {
        return this.page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return this.limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getSort() {
        return this.sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}
