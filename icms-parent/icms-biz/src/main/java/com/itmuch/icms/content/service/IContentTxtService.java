package com.itmuch.icms.content.service;

import com.itmuch.icms.content.domain.ContentTxt;

public interface IContentTxtService {

    public ContentTxt selectByContentId(Integer contentId);
}
