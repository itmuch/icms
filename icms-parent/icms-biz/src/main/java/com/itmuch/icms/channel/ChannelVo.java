package com.itmuch.icms.channel;

import java.util.Date;

public class ChannelVo {
    /**
     * id.
     */
    private Integer id;

    /**
     * 父栏目id.
     */
    private Integer parentId;

    /**
     * 栏目名称.
     */
    private String name;

    /**
     * 访问路径.
     */
    private String path;

    /**
     * 模板名称.
     */
    private String templateName;

    /**
     * 模板文件.
     */
    private String templateFile;

    /**
     * 优先级（排序）.
     */
    private Integer priority;

    /**
     * 单页标识 0否 1是 默认0.
     */
    private Byte singlePageFlag;

    /**
     * 是否显示 0否 1是 默认0.
     */
    private Byte displayFlag;

    /**
     * 创建时间.
     */
    private Date createTime;

    /**
     * 修改时间.
     */
    private Date updateTime;

    /**
     * 删除标识 0否 1是 默认0.
     */
    private Byte delFlag;

    /**
     * 栏目url
     */
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateFile() {
        return this.templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Byte getSinglePageFlag() {
        return this.singlePageFlag;
    }

    public void setSinglePageFlag(Byte singlePageFlag) {
        this.singlePageFlag = singlePageFlag;
    }

    public Byte getDisplayFlag() {
        return this.displayFlag;
    }

    public void setDisplayFlag(Byte displayFlag) {
        this.displayFlag = displayFlag;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }
}
