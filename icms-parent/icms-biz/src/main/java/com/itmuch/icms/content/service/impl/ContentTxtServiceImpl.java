package com.itmuch.icms.content.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.content.domain.ContentTxt;
import com.itmuch.icms.content.persistence.ContentTxtMapper;
import com.itmuch.icms.content.service.IContentTxtService;

@Service
public class ContentTxtServiceImpl implements IContentTxtService {
    @Resource
    private ContentTxtMapper contentTxtMapper;

    @Override
    public ContentTxt selectByContentId(Integer contentId) {
        return this.contentTxtMapper.selectByContentId(contentId);
    }

}
