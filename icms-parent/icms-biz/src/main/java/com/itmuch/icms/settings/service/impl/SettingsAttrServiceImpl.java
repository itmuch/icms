package com.itmuch.icms.settings.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.settings.domain.SettingsAttr;
import com.itmuch.icms.settings.persistence.SettingsAttrMapper;
import com.itmuch.icms.settings.service.ISettingsAttrService;

@Service
public class SettingsAttrServiceImpl implements ISettingsAttrService {
    @Resource
    private SettingsAttrMapper settingsAttrMapper;

    @Override
    public SettingsAttr selectBykey(String key) {
        return this.settingsAttrMapper.selectBykey(key);
    }

}
