package com.itmuch.icms.user.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.user.domain.UserInfo;
import com.itmuch.icms.user.persistence.UserInfoMapper;
import com.itmuch.icms.user.service.IUserInfoService;

@Service
public class UserInfoServiceImpl implements IUserInfoService {
    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public int deleteByIdReal(Integer id) {
        return this.userInfoMapper.deleteByIdReal(id);
    }

    @Override
    public int deleteById(Integer id) {
        return this.userInfoMapper.deleteById(id);
    }

    @Override
    public int insert(UserInfo record) {
        return this.userInfoMapper.insert(record);
    }

    @Override
    public UserInfo selectById(Integer id) {
        return this.userInfoMapper.selectById(id);
    }

    @Override
    public int updateById(UserInfo record) {
        return this.userInfoMapper.updateById(record);
    }

    @Override
    public UserInfo selectByUserId(Integer userId) {
        return this.userInfoMapper.selectByUserId(userId);
    }

}
