package com.itmuch.icms.settings.persistence;

import com.itmuch.icms.settings.domain.SettingsAttr;

public interface SettingsAttrMapper {
    /**
     * 通过id物理删除b_settings_attr的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除b_settings_attr的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表b_settings_attr中插入数据.
     */
    int insert(SettingsAttr record);

    /**
     * 通过id查询表b_settings_attr.
     */
    SettingsAttr selectById(Integer id);

    /**
     * 通过id修改表b_settings_attr.
     */
    int updateById(SettingsAttr record);

    SettingsAttr selectBykey(String key);
}