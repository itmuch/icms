package com.itmuch.icms.group.domain;

import java.io.Serializable;

public class GroupChannel implements Serializable {
    /**
     * id.
     */
    private Integer id;

    /**
     * f_group.id.
     */
    private Integer groupId;

    /**
     * f_channel.id.
     */
    private Integer channelId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getChannelId() {
        return this.channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }
}