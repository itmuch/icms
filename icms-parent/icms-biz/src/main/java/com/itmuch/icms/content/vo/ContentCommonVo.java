package com.itmuch.icms.content.vo;

import java.util.Date;

public class ContentCommonVo {
    /**
     * id.
     */
    private Integer id;

    /**
     * f_channel.id.
     */
    private Integer channelId;

    /**
     * f_user.id.
     */
    private Integer userId;

    /**
     * 标题.
     */
    private String title;

    /**
     * 标题颜色.
     */
    private String titleColor;

    /**
     * 标题加粗 0否 1是 默认0.
     */
    private Byte titleBoldFlag;

    /**
     * 标题图片 用于列表展示时.
     */
    private String titleImg;

    /**
     * 作者.
     */
    private String author;

    /**
     * 来源.
     */
    private String origin;

    /**
     * 来源url.
     */
    private String originUrl;

    /**
     * 摘要.
     */
    private String summary;

    /**
     * 发布时间.
     */
    private Date issueTime;

    /**
     * 状态 0审核中 1草稿 2审核通过 3回收站.
     */
    private Byte status;

    /**
     * 创建时间.
     */
    private Date createTime;

    /**
     * 修改时间.
     */
    private Date updateTime;

    /**
     * 删除标识 0否 1是 默认0.
     */
    private Byte delFlag;

    private String url;
    private String channelName;
    private String channelUrl;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChannelId() {
        return this.channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleColor() {
        return this.titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public Byte getTitleBoldFlag() {
        return this.titleBoldFlag;
    }

    public void setTitleBoldFlag(Byte titleBoldFlag) {
        this.titleBoldFlag = titleBoldFlag;
    }

    public String getTitleImg() {
        return this.titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginUrl() {
        return this.originUrl;
    }

    public void setOriginUrl(String originUrl) {
        this.originUrl = originUrl;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getIssueTime() {
        return this.issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public Byte getStatus() {
        return this.status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getChannelName() {
        return this.channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelUrl() {
        return this.channelUrl;
    }

    public void setChannelUrl(String channelUrl) {
        this.channelUrl = channelUrl;
    }
}
