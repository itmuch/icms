package com.itmuch.icms.channel.service;

import java.util.List;
import java.util.Set;

import com.itmuch.icms.channel.domain.Channel;

public interface IChannelService {

    List<Channel> selectTopChannels(Boolean displayOnly, Boolean singelPageFlag);

    List<Channel> selectChildChannelsById(Integer id, Boolean displayOnly, Boolean singelPageFlag);

    Channel selectByPath(String path);

    Channel selectById(Integer channelId);

    /**
     * 通过会员组，查询有权限投稿的栏目
     * @param groupId 会员组
     * @return 有权投稿的栏目列表
     */
    List<Channel> selectChannelsHasRightByGroupId(Integer groupId);

    /**
     * 查询所有的栏目
     * @return
     */
    List<Channel> selectChannelAll();

    /**
     * 修改栏目的排序
     * @param ids 栏目的id数组
     * @param priority 栏目的排序数组
     */
    void updatePriority(Integer[] ids, Integer[] priority);

    void deleteById(Integer id);

    /**
     * 查询一个栏目的子栏目个数
     * @param id
     * @return
     */
    Integer selectChildCountbyId(Integer id);

    int insert(Channel channel);

    int updateById(Channel channel);

    /**
     * 通过id, 查询该id下的子栏目id
     * @param id id
     * @param set 子栏目ids
     */
    public void selectChildChannelIds(Integer id, Set<Integer> set);
}
