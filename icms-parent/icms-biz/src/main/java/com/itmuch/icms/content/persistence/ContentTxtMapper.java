package com.itmuch.icms.content.persistence;

import com.itmuch.icms.content.domain.ContentTxt;

public interface ContentTxtMapper {
    /**
     * 通过id物理删除f_content_txt的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_content_txt的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_content_txt中插入数据.
     */
    int insert(ContentTxt record);

    /**
     * 通过id查询表f_content_txt.
     */
    ContentTxt selectById(Integer id);

    /**
     * 通过id修改表f_content_txt.
     */
    int updateById(ContentTxt record);

    ContentTxt selectByContentId(Integer contentId);

    int updateByContentId(ContentTxt record);
}