package com.itmuch.icms.content.domain;

import java.io.Serializable;
import java.util.Date;

public class Content implements Serializable {
    /**
     * id.
     */
    private Integer id;

    /**
     * f_channel.id.
     */
    private Integer channelId;

    /**
     * f_user.id.
     */
    private Integer userId;

    /**
     * 内容类型 0普通 1图文 2焦点 3头条.
     */
    private Byte typeId;

    /**
     * f_model.id.
     */
    private Byte modelId;

    /**
     * 推荐标识 0否 1是 默认0.
     */
    private Byte recommendFlag;

    /**
     * 标题.
     */
    private String title;

    /**
     * 标题颜色.
     */
    private String titleColor;

    /**
     * 标题加粗 0否 1是 默认0.
     */
    private Byte titleBoldFlag;

    /**
     * 标题图片 用于列表展示时.
     */
    private String titleImg;

    /**
     * 作者.
     */
    private String author;

    /**
     * 来源.
     */
    private String origin;

    /**
     * 来源url.
     */
    private String originUrl;

    /**
     * 摘要.
     */
    private String summary;

    /**
     * 发布时间.
     */
    private Date issueTime;

    /**
     * 优先级(排序).
     */
    private Integer priority;

    /**
     * 状态 0审核中 1审核通过 2草稿 3回收站.
     */
    private Byte status;

    /**
     * 创建时间.
     */
    private Date createTime;

    /**
     * 修改时间.
     */
    private Date updateTime;

    /**
     * 删除标识 0否 1是 默认0.
     */
    private Byte delFlag;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChannelId() {
        return this.channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Byte getTypeId() {
        return this.typeId;
    }

    public void setTypeId(Byte typeId) {
        this.typeId = typeId;
    }

    public Byte getModelId() {
        return this.modelId;
    }

    public void setModelId(Byte modelId) {
        this.modelId = modelId;
    }

    public Byte getRecommendFlag() {
        return this.recommendFlag;
    }

    public void setRecommendFlag(Byte recommendFlag) {
        this.recommendFlag = recommendFlag;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleColor() {
        return this.titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public Byte getTitleBoldFlag() {
        return this.titleBoldFlag;
    }

    public void setTitleBoldFlag(Byte titleBoldFlag) {
        this.titleBoldFlag = titleBoldFlag;
    }

    public String getTitleImg() {
        return this.titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginUrl() {
        return this.originUrl;
    }

    public void setOriginUrl(String originUrl) {
        this.originUrl = originUrl;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getIssueTime() {
        return this.issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Byte getStatus() {
        return this.status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }
}