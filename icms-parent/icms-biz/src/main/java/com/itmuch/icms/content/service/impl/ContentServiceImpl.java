package com.itmuch.icms.content.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.itmuch.core.constants.SessionConstants;
import com.itmuch.core.util.DozerUtil;
import com.itmuch.icms.channel.persistence.ChannelMapper;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.domain.ContentTxt;
import com.itmuch.icms.content.persistence.ContentMapper;
import com.itmuch.icms.content.persistence.ContentTxtMapper;
import com.itmuch.icms.content.service.IContentService;
import com.itmuch.icms.content.vo.ContentAdminSearchVo;
import com.itmuch.icms.content.vo.ContentContributeVo;
import com.itmuch.icms.content.vo.ContentSearchVo;

@Service
public class ContentServiceImpl implements IContentService {
    @Resource
    private ContentMapper contentMapper;

    @Resource
    private ContentTxtMapper contentTxtMapper;
    @Resource
    private ChannelMapper channelMapper;

    @Override
    public Content selectById(Integer id) {
        return this.contentMapper.selectById(id);
    }

    @Override
    public List<Content> selectByConditionPaged(ContentSearchVo contentSearchVo, PageBounds pageBounds) {
        // 包含子栏目下的文章，故而首先要查出所有的子栏目id

        if ((contentSearchVo.getContainChild() != null) && (contentSearchVo.getContainChild() == 1)) {
            if ((contentSearchVo.getChannelIds() != null) && (contentSearchVo.getChannelIds().length == 1)) {
                Set<Integer> set = new HashSet<Integer>();
                Integer cid = contentSearchVo.getChannelIds()[0];
                set.add(cid);
                this.selectChildChannelIds(cid, set);

                Integer[] idsContainChild = set.toArray(new Integer[set.size()]);
                contentSearchVo.setChannelIds(idsContainChild);
            }
        }

        return this.contentMapper.selectByConditionPaged(contentSearchVo, pageBounds);
    }

    public void selectChildChannelIds(Integer id, Set<Integer> set) {
        Set<Integer> list = this.channelMapper.selectChildIdById(id);
        set.addAll(list);
        if ((list != null) && !list.isEmpty()) {
            for (Integer item : list) {
                this.selectChildChannelIds(item, set);
            }
        }

    }

    @Override
    public List<Content> selectByIdsOrderBy(ContentSearchVo contentSearchVo) {
        return this.contentMapper.selectByIdsOrderBy(contentSearchVo);
    }

    @Override
    public int insert(Content content) {
        return this.contentMapper.insert(content);
    }

    @Override
    public void addContribute(ContentContributeVo vo, HttpSession session) {
        Content content = new Content();
        DozerUtil.map(vo, content);
        content.setCreateTime(new Date());
        content.setIssueTime(new Date());
        content.setUserId((Integer) session.getAttribute(SessionConstants.USER_ID));
        // TODO 以后要做成后台配置 是否需要审核
        content.setStatus((byte) 1);
        this.contentMapper.insert(content);

        ContentTxt contentTxt = new ContentTxt();
        contentTxt.setContentId(content.getId());
        contentTxt.setTxt(vo.getTxt());
        contentTxt.setCreateTime(new Date());
        this.contentTxtMapper.insert(contentTxt);
    }

    @Override
    public List<Content> selectByUserIdPaged(Integer userId, PageBounds pageBounds) {
        return this.contentMapper.selectByUserIdPaged(userId, pageBounds);
    }

    @Override
    public ContentContributeVo selectByIdAndUserId(Integer id, Integer userId) {
        return this.contentMapper.selectByIdAndUserId(id, userId);
    }

    @Override
    public void updateContribute(ContentContributeVo vo) {
        Content content = DozerUtil.map(vo, Content.class);
        this.contentMapper.updateById(content);

        ContentTxt txt = new ContentTxt();
        txt.setTxt(vo.getTxt());
        txt.setContentId(vo.getId());
        txt.setCreateTime(new Date());
        this.contentTxtMapper.updateByContentId(txt);
    }

    @Override
    public Integer selectCountbyChannelId(Integer channelId) {
        return this.contentMapper.selectCountbyChannelId(channelId);
    }

    @Override
    public List<Content> selectByConditionAdminPaged(ContentAdminSearchVo vo, PageBounds pageBounds) {
        return this.contentMapper.selectByConditionAdminPaged(vo, pageBounds);
    }

    @Override
    public Integer deleteByIds(List<Integer> ids) {
        return this.contentMapper.deleteByIds(ids);
    }

}
