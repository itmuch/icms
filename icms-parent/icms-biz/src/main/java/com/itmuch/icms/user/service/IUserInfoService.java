package com.itmuch.icms.user.service;

import com.itmuch.icms.user.domain.UserInfo;

public interface IUserInfoService {
    /**
     * 通过id物理删除f_user_info的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_user_info的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_user_info中插入数据.
     */
    int insert(UserInfo record);

    /**
     * 通过id查询表f_user_info.
     */
    UserInfo selectById(Integer id);

    /**
     * 通过id修改表f_user_info.
     */
    int updateById(UserInfo record);

    UserInfo selectByUserId(Integer userId);
}