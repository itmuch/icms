package com.itmuch.icms.content.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.vo.ContentAdminSearchVo;
import com.itmuch.icms.content.vo.ContentContributeVo;
import com.itmuch.icms.content.vo.ContentSearchVo;

public interface IContentService {

    public Content selectById(Integer id);

    /**
     * 按照条件，分页查询
     * @param contentSearchVo 查询条件
     * @param pageBounds 分页对象
     * @return 文章列表
     */
    public List<Content> selectByConditionPaged(ContentSearchVo contentSearchVo, PageBounds pageBounds);

    /**
     * 查询id在指定id数组中、指定排序的文章列表
     * @param contentSearchVo 查询条件
     * @return 文章列表
     */
    public List<Content> selectByIdsOrderBy(ContentSearchVo contentSearchVo);

    public int insert(Content content);

    public void addContribute(ContentContributeVo vo, HttpSession session);

    /**
     * 分页查询指定用户的文章列表
     * @param userId
     * @param pageBounds
     * @return
     */
    public List<Content> selectByUserIdPaged(Integer userId, PageBounds pageBounds);

    /**
     * 查询指定用户指定id的文章，用于修改稿件时候的判断
     * @param id 文章id
     * @param userId 用户id
     * @return 文章
     */
    public ContentContributeVo selectByIdAndUserId(Integer id, Integer userId);

    /**
     * 更新投稿.
     * @param vo
     */
    public void updateContribute(ContentContributeVo vo);

    public Integer selectCountbyChannelId(Integer id);

    /**
     * 后台根据条件查找
     * @param ContentAdminSearchVo 查询条件
     * @param pageBounds 分页对象
     * @return 文章列表
     */
    public List<Content> selectByConditionAdminPaged(ContentAdminSearchVo contentAdminSearchVo, PageBounds pageBounds);

    /**
     * 通过id字符串, 删除内容
     * @param ids 想要删除的id数组
     * @return 删除了多少条
     */
    public Integer deleteByIds(List<Integer> ids);
}
