package com.itmuch.icms.content.vo;

public class ContentSearchVo {
    private Integer[] ids;
    private Integer[] channelIds;
    private Integer img;
    private Integer[] excludeIds;
    private Integer status;

    private Boolean recommend;
    private Integer typeId;
    private Integer orderBy;
    private Integer containChild = 0;

    public Integer getImg() {
        return this.img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }

    public Boolean getRecommend() {
        return this.recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    public Integer getTypeId() {
        return this.typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer[] getChannelIds() {
        return this.channelIds;
    }

    public void setChannelIds(Integer[] channelIds) {
        this.channelIds = channelIds;
    }

    public Integer[] getExcludeIds() {
        return this.excludeIds;
    }

    public void setExcludeIds(Integer[] excludeIds) {
        this.excludeIds = excludeIds;
    }

    public ContentSearchVo(Integer[] channelIds, Boolean recommend, Integer orderBy, Integer typeId, Integer img, Integer[] excludeIds,
            Integer conntainChild, Integer status) {
        this.channelIds = channelIds;
        this.recommend = recommend;
        this.orderBy = orderBy;
        this.typeId = typeId;
        this.img = img;
        this.excludeIds = excludeIds;
        this.containChild = conntainChild;
        this.status = status;
    }

    public ContentSearchVo(Integer[] ids, Integer orderBy, Integer status) {
        this.ids = ids;
        this.orderBy = orderBy;
        this.status = status;
    }

    public ContentSearchVo() {
    }

    public Integer[] getIds() {
        return this.ids;
    }

    public void setIds(Integer[] ids) {
        this.ids = ids;
    }

    public Integer getContainChild() {
        return this.containChild;
    }

    public void setContainChild(Integer containChild) {
        this.containChild = containChild;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
