package com.itmuch.icms.settings.domain;

import java.io.Serializable;
import java.util.Date;

public class SettingsAttr implements Serializable {
    /**
     * id.
     */
    private Integer id;

    /**
     * 键名.
     */
    private String attrKey;

    /**
     * 键值.
     */
    private String attrValue;

    /**
     * 操作人id.
     */
    private Integer operatorId;

    /**
     * 创建时间.
     */
    private Date createTime;

    /**
     * 修改时间.
     */
    private Date updateTime;

    /**
     * 删除标识 0否 1是 默认0.
     */
    private Byte delFlag;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttrKey() {
        return this.attrKey;
    }

    public void setAttrKey(String attrKey) {
        this.attrKey = attrKey;
    }

    public String getAttrValue() {
        return this.attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public Integer getOperatorId() {
        return this.operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }
}