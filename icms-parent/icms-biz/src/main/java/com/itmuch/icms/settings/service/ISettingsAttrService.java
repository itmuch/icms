package com.itmuch.icms.settings.service;

import com.itmuch.icms.settings.domain.SettingsAttr;

public interface ISettingsAttrService {

    SettingsAttr selectBykey(String key);
}
