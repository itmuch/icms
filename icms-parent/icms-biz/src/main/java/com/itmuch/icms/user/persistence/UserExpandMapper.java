package com.itmuch.icms.user.persistence;

import com.itmuch.icms.user.domain.UserExpand;

public interface UserExpandMapper {
    /**
     * 通过id物理删除f_user_expand的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_user_expand的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_user_expand中插入数据.
     */
    int insert(UserExpand record);

    /**
     * 通过id查询表f_user_expand.
     */
    UserExpand selectById(Integer id);

    /**
     * 通过id修改表f_user_expand.
     */
    int updateById(UserExpand record);

    /**
     * 通过用户id查询f_user_expand.
     * @param userId
     * @return
     */
    UserExpand selectByUserId(Integer userId);
}