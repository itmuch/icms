package com.itmuch.icms.user.service;

import com.itmuch.icms.user.domain.User;

public interface IUserService {
    /**
     * 通过id物理删除f_user的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_user的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_user中插入数据.
     */
    int insert(User record);

    /**
     * 通过id查询表f_user.
     */
    User selectById(Integer id);

    /**
     * 通过id修改表f_user.
     */
    int updateById(User record);

    User selectByUsername(String username);
}
