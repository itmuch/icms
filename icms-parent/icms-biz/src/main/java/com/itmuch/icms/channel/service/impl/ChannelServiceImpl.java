package com.itmuch.icms.channel.service.impl;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.channel.domain.Channel;
import com.itmuch.icms.channel.persistence.ChannelMapper;
import com.itmuch.icms.channel.service.IChannelService;

@Service
public class ChannelServiceImpl implements IChannelService {
    @Resource
    private ChannelMapper channelMapper;

    @Override
    public List<Channel> selectTopChannels(Boolean displayOnly, Boolean singlePageFlag) {
        return this.channelMapper.selectTopChannels(displayOnly, singlePageFlag);
    }

    @Override
    public List<Channel> selectChildChannelsById(Integer id, Boolean displayOnly, Boolean singlePageFlag) {
        return this.channelMapper.selectChildChannelsById(id, displayOnly, singlePageFlag);
    }

    @Override
    public Channel selectByPath(String path) {
        return this.channelMapper.selectByPath(path);
    }

    @Override
    public Channel selectById(Integer id) {
        return this.channelMapper.selectById(id);
    }

    @Override
    public List<Channel> selectChannelsHasRightByGroupId(Integer groupId) {
        return this.channelMapper.selectChannelsHasRightByGroupId(groupId);
    }

    @Override
    public List<Channel> selectChannelAll() {
        return this.channelMapper.selectChannelAll();
    }

    @Override
    public void updatePriority(Integer[] ids, Integer[] priority) {
        for (int i = 0; i < ids.length; i++) {
            Channel channel = new Channel();
            channel.setId(ids[i]);
            channel.setPriority(priority[i]);
            this.channelMapper.updateById(channel);
        }
    }

    @Override
    public void deleteById(Integer id) {
        this.channelMapper.deleteById(id);
    }

    @Override
    public Integer selectChildCountbyId(Integer id) {
        return this.channelMapper.selectChildCountbyId(id);
    }

    @Override
    public int insert(Channel channel) {
        return this.channelMapper.insert(channel);
    }

    @Override
    public int updateById(Channel channel) {
        return this.channelMapper.updateById(channel);
    }

    @Override
    public void selectChildChannelIds(Integer id, Set<Integer> set) {
        Set<Integer> list = this.channelMapper.selectChildIdById(id);
        set.addAll(list);
        if ((list != null) && !list.isEmpty()) {
            for (Integer item : list) {
                this.selectChildChannelIds(item, set);
            }
        }

    }
}
