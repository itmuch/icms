package com.itmuch.icms.channel.persistence;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.itmuch.icms.channel.domain.Channel;

public interface ChannelMapper {
    /**
     * 通过id物理删除f_channel的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_channel的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_channel中插入数据.
     */
    int insert(Channel record);

    /**
     * 通过id查询表f_channel.
     */
    Channel selectById(Integer id);

    /**
     * 通过id修改表f_channel.
     */
    int updateById(Channel record);

    /**
     * 查询一级栏目
     * @param displayOnly 显示标识
     * @param singlePageFlag 单页标识
     * @return 栏目列表
     */
    List<Channel> selectTopChannels(@Param("displayOnly") Boolean displayOnly, @Param("singlePageFlag") Boolean singlePageFlag);

    /**
     * 查询指定id的子栏目
     * @param id id
     * @param displayOnly 显示标识
     * @param singlePageFlag 单页标识
     * @return 栏目列表
     */
    List<Channel> selectChildChannelsById(@Param("id") Integer id, @Param("displayOnly") Boolean displayOnly,
            @Param("singlePageFlag") Boolean singlePageFlag);

    Channel selectByPath(String path);

    List<Channel> selectChannelsHasRightByGroupId(@Param("groupId") Integer groupId);

    Set<Integer> selectChildIdById(Integer id);

    List<Channel> selectChannelAll();

    /**
     * 通过栏目id，查询该栏目的子栏目个数.
     * @param id 栏目id
     * @return 子栏目个数
     */
    Integer selectChildCountbyId(Integer id);
}