package com.itmuch.icms.content.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.itmuch.icms.content.domain.Content;
import com.itmuch.icms.content.vo.ContentAdminSearchVo;
import com.itmuch.icms.content.vo.ContentContributeVo;
import com.itmuch.icms.content.vo.ContentSearchVo;

public interface ContentMapper {
    /**
     * 通过id物理删除f_content的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_content的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_content中插入数据.
     */
    int insert(Content record);

    /**
     * 通过id查询表f_content.
     */
    Content selectById(Integer id);

    /**
     * 通过id修改表f_content.
     */
    int updateById(Content record);

    /**
     * 按照条件，分页查询
     * @param contentSearchVo 查询条件
     * @param pageBounds 分页对象
     * @return 文章列表
     */
    List<Content> selectByConditionPaged(ContentSearchVo contentSearchVo, PageBounds pageBounds);

    /**
     * 查询id在指定id数组中、指定排序的文章列表
     * @param contentSearchVo 查询条件
     * @return 文章列表
     */
    List<Content> selectByIdsOrderBy(ContentSearchVo contentSearchVo);

    /**
     * 分页查询指定用户的文章列表
     * @param userId
     * @param pageBounds
     * @return
     */
    List<Content> selectByUserIdPaged(Integer userId, PageBounds pageBounds);

    /**
     * 查询指定用户指定id的文章，用于修改稿件时候的判断
     * @param id 文章id
     * @param userId 用户id
     * @return 文章
     */
    ContentContributeVo selectByIdAndUserId(@Param("id") Integer id, @Param("userId") Integer userId);

    Integer selectCountbyChannelId(Integer channelId);

    /**
     * 后台根据条件查找
     * @param ContentAdminSearchVo 查询条件
     * @param pageBounds 分页对象
     * @return 文章列表
     */
    public List<Content> selectByConditionAdminPaged(ContentAdminSearchVo ContentAdminSearchVo, PageBounds pageBounds);

    /**
     * 通过id字符串, 删除内容
     * @param ids 需要删除的id数组
     * @return 删除了多少条
     */
    public Integer deleteByIds(List<Integer> ids);
}