package com.itmuch.icms.user.domain;

import java.io.Serializable;
import java.util.Date;

public class UserExpand implements Serializable {
    /**
     * id.
     */
    private Integer id;

    /**
     * f_user.id.
     */
    private Integer userId;

    /**
     * 注册时间.
     */
    private Date regTime;

    /**
     * 注册IP.
     */
    private String regIp;

    /**
     * 最后登录时间.
     */
    private Date lastTime;

    /**
     * 最后登录IP.
     */
    private String lastIp;

    /**
     * 登录次数.
     */
    private Integer loginCount;

    /**
     * 上传文件总大小KB.
     */
    private Integer uploadTotalSize;

    /**
     * 上传文件大小.
     */
    private Integer uploadSize;

    /**
     * 文件上传日期.
     */
    private Date uploadDate;

    /**
     * 创建时间.
     */
    private Date createTime;

    /**
     * 修改时间.
     */
    private Date updateTime;

    /**
     * 删除标识 0否 1是 默认否.
     */
    private Byte delFlag;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getRegTime() {
        return this.regTime;
    }

    public void setRegTime(Date regTime) {
        this.regTime = regTime;
    }

    public String getRegIp() {
        return this.regIp;
    }

    public void setRegIp(String regIp) {
        this.regIp = regIp;
    }

    public Date getLastTime() {
        return this.lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastIp() {
        return this.lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public Integer getLoginCount() {
        return this.loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Integer getUploadTotalSize() {
        return this.uploadTotalSize;
    }

    public void setUploadTotalSize(Integer uploadTotalSize) {
        this.uploadTotalSize = uploadTotalSize;
    }

    public Integer getUploadSize() {
        return this.uploadSize;
    }

    public void setUploadSize(Integer uploadSize) {
        this.uploadSize = uploadSize;
    }

    public Date getUploadDate() {
        return this.uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }
}