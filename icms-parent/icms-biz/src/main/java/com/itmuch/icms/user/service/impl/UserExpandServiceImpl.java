package com.itmuch.icms.user.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.itmuch.icms.user.domain.UserExpand;
import com.itmuch.icms.user.persistence.UserExpandMapper;
import com.itmuch.icms.user.service.IUserExpandService;

@Service
public class UserExpandServiceImpl implements IUserExpandService {
    @Resource
    private UserExpandMapper userExpandMapper;

    @Override
    public int deleteByIdReal(Integer id) {
        return this.userExpandMapper.deleteByIdReal(id);
    }

    @Override
    public int deleteById(Integer id) {
        return this.userExpandMapper.deleteById(id);
    }

    @Override
    public int insert(UserExpand record) {
        return this.userExpandMapper.insert(record);
    }

    @Override
    public UserExpand selectById(Integer id) {
        return this.userExpandMapper.selectById(id);
    }

    @Override
    public int updateById(UserExpand record) {
        return this.userExpandMapper.updateById(record);
    }

    @Override
    public UserExpand selectByUserId(Integer userId) {
        return this.userExpandMapper.selectByUserId(userId);
    }

}
