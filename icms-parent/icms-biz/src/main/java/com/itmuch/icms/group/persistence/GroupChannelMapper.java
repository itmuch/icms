package com.itmuch.icms.group.persistence;

import com.itmuch.icms.group.domain.GroupChannel;

public interface GroupChannelMapper {
    /**
     * 通过id物理删除f_group_channel的数据.
     */
    int deleteByIdReal(Integer id);

    /**
     * 通过id逻辑删除f_group_channel的数据.
     */
    int deleteById(Integer id);

    /**
     * 向表f_group_channel中插入数据.
     */
    int insert(GroupChannel record);

    /**
     * 通过id查询表f_group_channel.
     */
    GroupChannel selectById(Integer id);

    /**
     * 通过id修改表f_group_channel.
     */
    int updateById(GroupChannel record);
}