package com.itmuch.core.exception;

public class BizException extends Exception {

    private static final long serialVersionUID = -290372483676756273L;

    public BizException() {
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }
}
