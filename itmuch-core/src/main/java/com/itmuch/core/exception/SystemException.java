package com.itmuch.core.exception;

public class SystemException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7485843848900236252L;

    public SystemException() {
    }

    /** 
     * @param message 
     */
    public SystemException(String message) {
        super(message);
    }

    /** 
     * @param cause 
     */
    public SystemException(Throwable cause) {
        super(cause);
    }

    /** 
     * @param message 
     * @param cause 
     */
    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
