package com.itmuch.core.web.servlet.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itmuch.core.exception.ErrorCodeConstant;
import com.itmuch.core.web.converter.Result;

public class CustomSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSimpleMappingExceptionResolver.class);

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LOGGER.error("控制器异常.", ex);

        // Expose ModelAndView for chosen error view.
        String viewName = this.determineViewName(ex, request);
        // JSP格式返回
        if (viewName != null) {
            if (!((request.getHeader("accept").indexOf("application/json") > -1) || ((request.getHeader("X-Requested-With") != null) && (request
                    .getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1)))) {
                // 如果不是异步请求
                // Apply HTTP status code for error views, if specified.
                // Only apply it if we're processing a top-level request.
                Integer statusCode = this.determineStatusCode(request, viewName);
                if (statusCode != null) {
                    this.applyStatusCodeIfPossible(request, response, statusCode);
                }
                // request.setAttribute("Exception", ex.getMessage());
                return this.getModelAndView(viewName, ex, request);
            } else {// JSON格式返回

                Result result = new Result(null);

                if (ex instanceof MyBatisSystemException) {
                    result.setErrorCode(ErrorCodeConstant.MYBATIS_SYSTEM_ERROR);
                } else {
                    result.setErrorCode(ErrorCodeConstant.UNKNOW_ERROR_CODE);
                }
                result.setMsg(ex.getMessage());
                byte[] byteArray = null;
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    byteArray = objectMapper.writeValueAsBytes(result);
                    FileCopyUtils.copy(byteArray, response.getOutputStream());
                } catch (IOException e) {
                    LOGGER.error("处理错误编码出错", e);
                }
                // 本想直接return null，但是发现return null时，会将抛出的异常打印到控制台，干脆return一个新的视图了。
                return new ModelAndView();
            }
        } else {
            return null;
        }
    }
}