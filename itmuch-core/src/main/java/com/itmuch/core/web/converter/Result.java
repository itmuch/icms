package com.itmuch.core.web.converter;

import com.itmuch.core.exception.ErrorCodeConstant;

/**
 * 返回结果包装类.
 * 
 * @author wanghaijun
 * 
 */
public class Result {
    private boolean success = true;
    private int errorCode = ErrorCodeConstant.SUCCESS_CODE;
    private String msg = "操作成功";
    private Object data;

    public Result(Object object) {
        if (object == null) {
            this.fail(ErrorCodeConstant.DATA_NOT_FOUND);
        } else {
            this.success = true;
            this.errorCode = ErrorCodeConstant.SUCCESS_CODE;
            this.msg = "操作成功";
            this.data = object;
        }
    }

    public void fail() {
        this.errorCode = ErrorCodeConstant.UNKNOW_ERROR_CODE;
        this.success = false;
        this.msg = "操作失败";
        this.data = null;
    }

    public void fail(int errorCode) {
        this.success = false;
        this.errorCode = errorCode;
        this.msg = "操作失败";
        this.data = null;
    }

    public void fail(int errorCode, String msg) {
        this.success = false;
        this.errorCode = errorCode;
        this.msg = msg;
        this.data = null;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return this.success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return this.errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return this.msg;
    }

    /**
     * @param msg
     *            the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the data
     */
    public Object getData() {
        return this.data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }

}
