package com.itmuch.core.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StringUtil {
    public static String YESTERDAY = "昨日";
    public static String BEFORE_YESTERDAY = "前日";
    public static String LAST_WEEK = "上周同期";
    public static String UNKNOWN = "未知时间";

    /**
     * 为日期起别名
     * @param date
     * @return
     */
    public static String aliasDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (date == null) {
            return UNKNOWN;
        } else {
            String dateString = format.format(date);

            if (parseDateString(format, -1).equals(dateString)) {
                return YESTERDAY;
            } else if (parseDateString(format, -2).equals(dateString)) {
                return BEFORE_YESTERDAY;
            } else if (parseDateString(format, -7).equals(dateString)) {
                return LAST_WEEK;
            } else {
                return dateString;
            }
        }

    }

    /**
     * 通过偏移量以及指定的format，将该天日期格式化，例如offset=-1，则可将昨天格式化
     * @param format 日期格式化
     * @param offset 偏移量
     * @return
     */
    public static String parseDateString(SimpleDateFormat format, int offset) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, offset);
        return format.format(cal.getTime());
    }

    /**
     * 格式化指定时间为指定字符串
     * @param format 格式化
     * @param date 时间
     * @return
     */
    public static String formatDate(SimpleDateFormat format, Date date) {
        if (date == null) {
            return "";
        } else {
            return format.format(date);
        }
    }
}
