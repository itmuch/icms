package com.itmuch.core.util;

import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.itmuch.core.exception.ErrorCodeConstant;
import com.itmuch.core.web.converter.Result;

public class ErrorMsgUtil {
    public static String msgConverter(List<ObjectError> errors) {
        StringBuffer msg = new StringBuffer();
        if (errors != null) {
            for (ObjectError error : errors) {
                msg.append(error.getDefaultMessage() + " ");
            }
        }
        return msg.toString();
    }

    public static Result invalidResult(BindingResult bindingResult) {
        return ErrorMsgUtil.error(ErrorMsgUtil.msgConverter(bindingResult.getAllErrors()), ErrorCodeConstant.PARAMTER_ERROR_CODE);
    }

    public static Result error(String msg, int errorCode) {
        Result result = new Result(null);
        result.setMsg(msg);
        result.setErrorCode(errorCode);
        result.setSuccess(false);
        return result;
    }

    //    /**
    //     * 为分页做错误提示.
    //     * @param msg 消息提示
    //     * @param errorCode 错误码
    //     * @return
    //     */
    //    public static PageList errorPageList() {
    //        Paginator paginator = new Paginator(1, 1, 0);
    //        PageList pageList = new PageList(paginator);
    //        return pageList;
    //
    //    }
}
